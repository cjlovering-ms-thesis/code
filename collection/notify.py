import argparse
import boto3
import pandas as pd
from typing import List

region_name = 'us-east-1'
aws_access_key_id = 'YOUR_ACCESS_ID'
aws_secret_access_key = 'YOUR_SECRET_KEY'

endpoint_url = 'https://mturk-requester-sandbox.us-east-1.amazonaws.com'
endpoint_url = 'https://mturk-requester.us-east-1.amazonaws.com'

def get_worker_ids(data_path: str) -> List[str]:
  """Get ids of qualified workers. """
  data = pd.read_csv(data_path)
  mask = data['CURRENT-Qualified English Survey'] > 0
  data = data[mask]
  return list(data['Worker ID'].values)

def main(config):
  client = boto3.client(
      'mturk',
      endpoint_url=endpoint_url,
      region_name=region_name
  )
  message = """
    Hello!

    We wanted to let you know that you recently completed our qualification survey 
    and now have access to a HIT with a reward of $1. The hit can be found via the 
    requester name 'Charles Lovering' and the qualification is called 
    'Qualified English Survey'. We will be releasing another similar HIT, available to
    only those with this qualification, after this HIT is complete.
  """
  worker_ids = get_worker_ids(config.data_path)
  response = client.notify_workers(
      Subject='string',
      MessageText=message,
      WorkerIds=worker_ids
  )
  print(response)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--data_path', type=str, default="../../data/workers.csv")
  parser.add_argument('--qualification', type=str, default="CURRENT-Qualified English Survey")
  main(parser.parse_args())