init:
	sudo apt install python3-pip
	pip3 install --upgrade pip
	pip install -r requirements.txt
	cd ..
	git clone https://cjlovering@bitbucket.org/cjlovering-ms-thesis/data.git
	cd code

test:
	# tests
	py.test tests/

prepare_corpus:
	# process dataset
	python3 -m lib.utils.process.py
	# Embedding corpus. (~1hr).
	python3 -m lib.utils.embed_corpus.py

create_gold_questions:
	python3 -m lib.utils.gold_questions

qualifier:
	python3 -m lib.utils.qualifier
	cat ../data/tasks/qualifier.csv | sed "s#b'b##g"  | sed 's#bb##g' | sed "s#b'##g"  | sed "s#'"#"#g" > ../data/tasks/out.csv

grade_qualifier:
	python3 -m lib.utils.grade_qualifier

analyze_qualifier:
	python3 -m lib.utils.grade_qualifier --analyze --out_path ../data/output/qualifier_analysis.csv

create_dataset:
	python3 -m lib.utils.create_dataset

annotate_gold:
	python3 -m lib.utils.annotate_gold

analyze_overview:
	python3 -m lib.analysis.overview

compare:
	python3 -m lib.utils.annotate_gold --in_path ../data/output/control_results.csv --out_path ../data/output/cr.csv
	python3 -m lib.utils.annotate_gold --in_path ../data/output/qualified_results.csv --out_path ../data/output/qr.csv
	python3 -m lib.analysis.overview --annotated_name cr.csv --analysis_name control_analysis.csv
	python3 -m lib.analysis.overview --annotated_name qr.csv --analysis_name qualified_analysis.csv
	python3 -m lib.analysis.hitwise --annotated_name cr.csv --analysis_name control_analysis.csv
	python3 -m lib.analysis.hitwise --annotated_name qr.csv --analysis_name qualified_analysis.csv

prepare_dataset:
	# generate annotated data
	./scripts/splits.sh
