from .context import blackjack

def test_blackjack_1():
  items = [ 5, 25, 100 ]
  target = 90
  item_size = lambda x:x
  assert [ 5, 25 ] == blackjack(items, target, item_size=item_size)

def test_blackjack_2():
  items = [ 5, 25, 100 ]
  target = 4
  item_size = lambda x:x
  assert [ ] == blackjack(items, target, item_size=item_size)

def test_blackjack_3():
  items = [ 4, 25, 100, 85 ]
  target = 104
  item_size = lambda x:x
  assert [ 4, 100 ] == blackjack(items, target, item_size=item_size)

def test_blackjack_list():
  items = [ 
    [ 1, 2, 4 ], # 3
    [ 1, 2, 4, 1, 2, 4, 1, 2, 4 ],  # 9
    [ 1, 2, 4, 1, 2, 4 ], # 6
  ]
  target = 12
  assert [ 
    [ 1, 2, 4 ],
    [ 1, 2, 4, 1, 2, 4, 1, 2, 4 ]
   ] == blackjack(items, target)