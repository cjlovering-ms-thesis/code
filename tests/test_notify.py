from .context import get_worker_ids

def test_get_worker_ids():
  out = get_worker_ids("tests/data/test_workers.csv")
  assert len(out) == 46