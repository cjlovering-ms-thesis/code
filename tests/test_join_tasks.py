from .context import join_tasks

import numpy as np

def test_join_tasks():
  left = [ list(np.arange(15)) * 8]
  right = [ 
    list(np.arange(15)), 
    list(np.arange(15)) ]
  out = join_tasks(left, right)
  target = [ list(np.arange(15)) * 10 ]
  assert out == target