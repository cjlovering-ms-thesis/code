from .context import create_tasks, get_partition

def test_get_partition():
  data = [1, 2, 3, 4, 5]
  assert get_partition(data, 0, 2) == [1, 2]
  assert get_partition(data, 3, 2) == [4, 5]
  assert get_partition(data, 5, 2) == [4, 5]

def test_create_tasks():
  data = [[1,2,3] for _ in range(5)]
  assert create_tasks(data, 5) == [[1,2,3]*5]
  assert create_tasks(data, 1) == data
  assert create_tasks(data, 2) == [[1,2,3,1,2,3] for _ in range(3)]
  data = [[1,2,3] for _ in range(4)]
  assert create_tasks(data, 2) == [[1,2,3,1,2,3] for _ in range(2)]
