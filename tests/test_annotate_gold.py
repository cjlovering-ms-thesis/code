from .context import annotate_gold_questions

import pandas as pd

def test_annotate_gold_questions():
  annotate_gold_questions("tests/data/test_mturk_results.csv", "tests/data/test_mturk_annotated_results.csv")
  data = pd.read_csv("tests/data/test_mturk_annotated_results.csv")
  reject_message = 'Unfortunately, too many of the answers you submitted for this HIT are incorrect. As noted in the instructions, we require a sufficiently high accuracy in order to accept your work.'

  # Approve when both gold std questions are correct.
  assert data.loc[0, "Approve"] == 'x'
  assert pd.isnull(data.loc[0, "Reject"])

  # Reject when both a gold std question is wrong.
  assert pd.isnull(data.loc[1, "Approve"])
  assert data.loc[1, "Reject"] == reject_message

  # Reject when both a gold std question is wrong.
  assert pd.isnull(data.loc[2, "Approve"])
  assert data.loc[2, "Reject"] == reject_message

  # Reject when both gold std questions are wrong.
  assert pd.isnull(data.loc[3, "Approve"])
  assert data.loc[3, "Reject"] == reject_message
