#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""IO for converting and downloading files.
"""

from __future__ import print_function

import exceptions
import os
import sys
import getopt
import urllib2
import re
import json
import wget

from cStringIO import StringIO
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter, HTMLConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage

PAPER_PATH = "static/papers/"
TXT_PATH = "{0}txt/".format(PAPER_PATH)
HTML_PATH = "{0}html/".format(PAPER_PATH)

"""Currently the file paths are incorrect."""

def trim_txt(begin_path, name):
    """Trims a txt file given a name w/o path. """
    with open("{0}txt/{1}.txt".format(begin_path, name), 'r') as rf:
        with open("{0}txt/trimmed_{1}.txt".format(begin_path, name), 'w') as wf:
            content = rf.readlines()
            new_lines = ""
            for line in content:
                if len(line) < 10:
                    line = re.sub('\s+', '\n', line)
                    if line is not "\n":
                        continue
                if line is "\n":
                    tmp = line
                else:
                    tmp = line.replace("\n", " ")
                new_lines = new_lines + tmp
            new_lines = re.sub('\n\n\n+', '\n', new_lines)
            wf.write(new_lines)

def convert_local(id):
    """Converts a local file. """
    rel_path = os.path.dirname(os.path.abspath(__file__))
    begin_path = "{0}/../static/papers/".format(rel_path)
    write_path = "{0}/../static/papers/pdf/{1}.pdf".format(rel_path, id)
    convert_path = "{0}/../static/papers/txt/{1}.txt".format(rel_path, id)
    views = ["txt"]
    for view in views:
        text = convert(write_path, view)
        with open(convert_path, 'w') as f:
            f.write(text)
        trim_txt(begin_path, id)


def convert_external_pdf(link, id):
    """Convert external pdf. """
    rel_path = os.path.dirname(os.path.abspath(__file__))
    begin_path = "{0}/../static/papers/".format(rel_path)
    write_path = "{0}/../static/papers/pdf/{1}.pdf".format(rel_path, id)
    convert_path = "{0}/../static/papers/txt/{1}.txt".format(rel_path, id)
    worked = save_external_file(write_path, link, id, "pdf")

    if not worked:
        return False, False

    views = ["txt"]
    for view in views:
        text = convert(write_path, view)
        if text is "":
            return True, False
        with open(convert_path, 'w') as f:
            f.write(text)
        trim_txt(begin_path, id)
        return True, True

def convert_local_pdf(id):
    """Converts a local pdf. """
    rel_path = os.path.dirname(os.path.abspath(__file__))
    begin_path = "{0}/../static/papers/".format(rel_path)
    write_path = "{0}/../static/papers/pdf/{1}.pdf".format(rel_path, id)
    convert_path = "{0}/../static/papers/txt/{1}.txt".format(rel_path, id)

    views = ["txt"]
    for view in views:
        text = convert(write_path, view)
        if text is "":
            return True, False
        with open(convert_path, 'w') as f:
            f.write(text)
        trim_txt(begin_path, id)
        return True, True

def save_external_file(write_path, link, id, view):
    """Loads and stores an external file. """
    try:
        wget.download(link, out=write_path, bar=None)
        return True
    except RuntimeError as re:
        return False


def convert(fname, view, pages=None):
    """Converts pdf, returns its text content as a string. """
    try:
        if not pages:
            pagenums = set()
        else:
            pagenums = set(pages)

        with open(fname, 'rb') as infile:
            output = StringIO()
            manager = PDFResourceManager()
            if view == "txt":
                converter = TextConverter(manager, output, laparams=LAParams())
            else:
                converter = HTMLConverter(manager, output, laparams=LAParams())
            interpreter = PDFPageInterpreter(manager, converter)
            for number, page in enumerate(PDFPage.get_pages(infile, pagenums)):
                interpreter.process_page(page)
            text = output.getvalue()
            converter.close()
            output.close()
            return text
    except:
        return ""

if __name__ == '__main__':
    convert_local_files()
