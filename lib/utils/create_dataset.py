from __future__ import print_function

import argparse
import unicodecsv as csv
import json
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import random
import torch

from collections import defaultdict
from itertools import combinations
from typing import Dict, List
from tqdm import tqdm
from unidecode import unidecode

from . import claim_data
from . import clean
from . import documents
from . import encode
from . import pre_annotation_config
from . import multiple_choice
from . import tasks

def cosine_similarity(evidence, claim):
  """Compute the cosine distance between vectors. """
  return np.dot(evidence, claim) / (np.linalg.norm(evidence, axis=1) * np.linalg.norm(claim))

def rank_sentences(datum, data_path):
  """Rank sentences, returning ordering.

      Args:
          datum:
              'claim': <string>,
              'sentences': [<string>...],
              'document_r_id': <uuid>,
              'document_id': <uuid>,
              'index': <int>
              'reference_id': <uuid>,

      Returns:
          ranking: values of related sentences
          scores: raw scores
  """
  claim, sentences = datum['claim'], datum['sentences']
  document_id = datum['document_id']
  document_r_id = datum['document_r_id']
  index = datum['index']

  claim_e = documents.get_embedding(document_id, index, data_path)
  sentences_e = documents.get_document_embedding(document_r_id, data_path)

  scores = cosine_similarity(sentences_e, claim_e)
  ranking = np.argsort(scores)
  return ranking

def get_k(datum, k_top_similar, embedding_path):
  """Gets k best sentences from datum
  """
  ranking = rank_sentences(datum, embedding_path)
  valid_ranking = clean.filter_evidence_idx(datum, ranking)
  best_idx = valid_ranking[-k_top_similar:]
  return best_idx

def get_questions(datum, config) -> List[List[int]]:
  """Returns a flattened list of questions.
  """
  k_top_similar = min(config.k_top_similar, len(datum['sentences']))
  ranked_idx = get_k(datum, k_top_similar, config.embedding_path)
  if len(ranked_idx) < config.multiple_choice_options:
    return []
  questions = []
  for _ in range(config.sets_needed):
    questions.extend(
      get_multiple_choice(ranked_idx, k_top_similar, config.multiple_choice_options))
  return questions

def get_multiple_choice(sentence_idx: List[int], k_top_similar: int=25, multiple_choice_options: int=4):
  """Returns a list of MC questions.
  """
  k_top_similar = min(k_top_similar, len(sentence_idx))
  permutation = np.random.choice(k_top_similar, k_top_similar, replace=False)
  sentence_permutation = [sentence_idx[idx] for idx in permutation]
  return [
    get_question(sentence_permutation, i, multiple_choice_options)
    for i in range(0, k_top_similar, multiple_choice_options)
  ]

def get_question(sentence_permutation: List, i: int, multiple_choice_options: int) -> List[int]:
  """Selects consecutive sentences as 4 options in a multiple choice question.

  Returns:
    (List[int]): list of indices.
  """
  return get_partition(sentence_permutation, i, multiple_choice_options)

def get_task(questions, i, questions_per_task):
  """Selects questions_per_task sentences as to be grouped together as a task.
  """
  return get_partition(questions, i, questions_per_task)

def get_partition(data, i, k):
  """Selects k items from a list starting at position i.
  """
  if i + k >= len(data):
    return data[-k:]
  return data[i:i+k]

def format_task(questions):
  """Flattens a list of questions into a single task.
  """
  out = []
  for q in questions:
    out.extend(q)
  return out

def header(questions_per_task, gold_per_task, multiple_choice_options):
  """Generates the header for a MTURK csv given a config. """
  # assuming multiple choice is set to 4
  """
  document_id_{}, 'claim_index_{}', 'reference_id_{}', 'document_r_id_{}', c_{}, c_idx_{}, e_{}_{}..., e_idx_{}_{}..., 'is_gold_{}', 'gold_answer_{}'
  """
  out = []
  for question_idx in range(questions_per_task+gold_per_task):
    remaining = ['document_id_{}', 'claim_index_{}', 'reference_id_{}', 'document_r_id_{}']
    remaining_formatted = [ r.format(question_idx) for r in remaining ]
    out.extend(remaining_formatted)
    out.append('c_{}'.format(question_idx))
    end = []
    for multiple_choice_idx in range(multiple_choice_options):
      out.append('e_{}_{}'.format(str(question_idx), str(multiple_choice_idx)))
      end.append('e_idx_{}_{}'.format(str(question_idx), str(multiple_choice_idx)))
    end.append('is_gold_{}'.format(question_idx))
    end.append('gold_answer_{}'.format(question_idx))
    out.extend(end)
  return out

def format_questions(data, config):
  formatted_questions = []
  for d in tqdm(data, desc="Formatting questions"):
    formatted_questions.extend(multiple_choice.format_question(d, get_questions(d, config), config))
  random.shuffle(formatted_questions)
  return formatted_questions

def create_tasks(formatted_questions: List, questions_per_task: int) -> List[List]:
  assert questions_per_task > 0
  return [
    format_task(get_task(formatted_questions, i, questions_per_task))
    for i in tqdm(range(0, len(formatted_questions), questions_per_task), desc="Creating tasks")
  ]

def main(config):
  # get data
  data = claim_data.get_claim_data(config)

  # format questions
  formatted_questions = format_questions(data, config)

  # create tasks
  tasks = create_tasks(formatted_questions, config.questions_per_task)

  # gold standard
  gold = pd.read_csv(config.data_path+'tasks/gold_questions_full.csv', sep=",", quotechar='"', quoting=csv.QUOTE_MINIMAL).values

  # join tasks
  joined_tasks = tasks.join_tasks(tasks, gold)

  # saving tasks
  with open(config.data_path+'tasks/mturk_experiment_all.csv', 'wb') as csvfile:
    writer = csv.writer(csvfile, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL, encoding='utf-8') 
    writer.writerow(header(config.questions_per_task, config.gold_per_task, config.multiple_choice_options))
    writer.writerows([encode.encode_task(t) for t in tqdm(joined_tasks, desc="Saving tasks")])

  with open(config.data_path+'tasks/mturk_experiment_sample.csv', 'wb') as csvfile:
    writer = csv.writer(csvfile, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(header(config.questions_per_task, config.gold_per_task, config.multiple_choice_options))
    writer.writerows([encode.encode_task(t) for t in tqdm(joined_tasks[:18], desc="Saving tasks")])

if __name__ == "__main__":
  config = pre_annotation_config.get_config()
  main(config)
