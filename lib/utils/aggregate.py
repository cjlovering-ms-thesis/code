import argparse
import json
import random
import uuid

import pandas as pd

from collections import defaultdict
from sklearn.model_selection import train_test_split

from extract import balance_data, stratify_split, format_data, transform_data, to_label, clean
from index import build_index, fill_index

def fill_data(index):
  """Generates additional labeled pairings. """
  data = []

  for task_id, current in index.items():
    hid, cdi, idx, edi = task_id
    
    relevant = current['relevant']
    irrelevant = current['irrelevant']

    for r_idx in random.sample(relevant, min(5, len(relevant)) ):
      for ir_idx in random.sample(irrelevant,  min(5, len(irrelevant)) ):
        data.append( [ hid + "+" + str(uuid.uuid4()), cdi, idx, edi, r_idx, ir_idx, 'optionA' ] )
  
  return data

def label_filled_data(data):
  """Adds labels to new data. """
  out = []
  for d in data:
    copy = [*d]
    copy[-1] = to_label(d[-1])
    out.append(copy)
  return out

def save_file(X, y, name, out_path):
  """Saves data into files of the given name. """
  with open("{}X_{}.json".format(out_path, name), 'w') as jf:
    json.dump(X, jf)
  with open("{}y_{}.json".format(out_path, name), 'w') as jf:
    json.dump(y, jf)

def main(config):
  data = pd.read_csv(config.in_path+"answers.csv")
  selected = data[["HITId", "Input.document_id", "Input.index", "Input.document_r_id", "Input.evidence_index_1", "Input.evidence_index_2", "Answer.choice"]]
  
  # Add new
  index = build_index(selected.values)
  filled_index = fill_index(index, config.document_path)
  filled_data = fill_data(index)
  labeled_data = label_filled_data(filled_data)

  # stratify across documents to prevent data leakage.
  data_train, temp = stratify_split(labeled_data, test_size=0.30)
  data_val, data_test = stratify_split(temp, test_size=0.50)

  # format data.
  X_train, y_train = format_data(data_train, document_path=config.document_path)
  X_val, y_val = format_data(data_val, document_path=config.document_path)
  X_test, y_test = format_data(data_test, document_path=config.document_path)

  if config.clean:
    X_train, y_train = clean(X_train, y_train)
    X_val, y_val = clean(X_val, y_val)
    X_test, y_test = clean(X_test, y_test)

  if config.balance:
    X_train, y_train = balance_data(X_train, y_train)
    X_val, y_val = balance_data(X_val, y_val)
    X_test, y_test = balance_data(X_test, y_test)

  if config.verbose:
    print("train: {}, test: {}, validation: {}".format(len(X_train), len(X_test), len(X_val)))
    print("train: {}, test: {}, validation: {}".format(len(y_train), len(y_test), len(y_val)))

  save_file(X_train, y_train, 'train', out_path=config.out_path)
  save_file(X_test, y_test, 'test', out_path=config.out_path)
  save_file(X_val, y_val, 'val', out_path=config.out_path)

if __name__ == "__main__":

  parser = argparse.ArgumentParser()
  
  # utils
  parser.add_argument('--document_path', type=str, default="../data/")
  parser.add_argument('--in_path', type=str, default="../data/results/1/")
  parser.add_argument('--out_path', type=str, default="../data/results/1-infer/")
  parser.add_argument('--balance', type=bool, default=True)
  parser.add_argument('--verbose', type=bool, default=True)
  parser.add_argument('--clean', type=bool, default=True)

  config = parser.parse_args()

  # extract data and vote
  main(config)
