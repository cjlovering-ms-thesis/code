import argparse
import pandas as pd

import numpy as np
import pandas as pd
import itertools
from tqdm import tqdm
import csv

from sklearn.metrics import cohen_kappa_score, roc_auc_score, f1_score, accuracy_score, average_precision_score

from . import claim_data
from . import create_dataset
from . import encode
from . import pre_annotation_config

def analyze_qualifier(in_path: str, out_path: str):
  data_in = pd.read_csv(in_path)
  data_out = data_in.apply(process_row, axis=1).apply(report, axis=1)
  data_out.to_csv(out_path, index=False)

def grade_qualifier(in_path: str, out_path: str, workers_path: str):
  data_in = pd.read_csv(in_path)
  grades = data_in.apply(process_row, axis=1).apply(report, axis=1).apply(grade, axis=1)
  mask = grades['Update'] == True
  data_out = grades[mask]
  data_out.to_csv(out_path, index=False, columns=['Worker ID', 'UPDATE-Qualified English Survey'], float_format='%.f')

def process_row(row, q: int=10):
  y_true = [row["Input.gold_answer_{}".format(str(q_idx))] for q_idx in range(q)]
  y_score = [row["Answer.e_{}".format(str(q_idx))] for q_idx in range(q)]
  gold = [row["Input.is_gold_{}".format(str(q_idx))] for q_idx in range(q)]
  return pd.Series({
    'WorkerId': row['WorkerId'],
    'gold': gold,
    'y_true': y_true,
    'y_score': y_score
  })

def report(row):
  WorkerId, y_true, y_score, gold = row['WorkerId'], np.array(row['y_true']), np.array(row['y_score']), np.array(row['gold'])
  y_true_gold, y_true_hard = y_true[gold], y_true[~gold]
  y_score_gold, y_score_hard = y_score[gold], y_score[~gold]
  return pd.Series({
    'WorkerId': WorkerId, 
    'Accuracy': accuracy_score(y_true_hard, y_score_hard),
    'CohenKappa': cohen_kappa_score(y_true_hard, y_score_hard),
    'GoldAccuracy': accuracy_score(y_true_gold, y_score_gold),
    'GoldCohenKappa': cohen_kappa_score(y_true_gold, y_score_gold),
  })

def grade(row):
  def threshold_7_4_18(accuracy_score: float, gold_accuracy_score: float) -> bool:
    """Must get all gold correct and at least 5 correct. """
    return accuracy_score >= (5/6) and gold_accuracy_score > 0.999
  if not threshold_7_4_18(row['Accuracy'], row['GoldAccuracy']):
    return pd.Series({
      'Worker ID': row['WorkerId'],
      'Update': False
    })
  return pd.Series({
    'Worker ID': row['WorkerId'], 
    'UPDATE-Qualified English Survey': 1,
    'Update': True
  })

def main(config):
  if config.analyze:
    analyze_qualifier(config.in_path, config.out_path)
  else:
    grade_qualifier(config.in_path, config.out_path, config.workers_path)

def _join():
  batch_1 = pd.read_csv("../../../data/output/qualifier_01.csv")
  batch_2 = pd.read_csv("../../../data/output/qualifier_02.csv")
  batch_3 = pd.read_csv("../../../data/output/qualifier_03.csv")
  qr = qualifier_results = batch_1.append(batch_2).append(batch_3)
  qr.to_csv("../../../data/output/qualifier.csv", index=False)

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('--in_path', type=str, default="../data/output/qualifier.csv")
  parser.add_argument('--workers_path', type=str, default="../data/output/workers.csv")
  parser.add_argument('--out_path', type=str, default="../data/output/qualifier_annotated.csv")
  parser.add_argument('--analyze', action='store_true')
  config = parser.parse_args()

  main(config)