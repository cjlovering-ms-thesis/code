import argparse
import random

import pandas as pd

from collections import defaultdict

from documents import get_sentence, save_file
from extract import stratify_split, to_label
from index import build_index, fill_index

def annotate_pointwise(data, document_path):
  collated = defaultdict(list)
  for datum in data:
    hid, c_did, c_idx, e_did, e_idx_1, e_idx_2, answer = datum

    if to_label(answer):
      collated[(hid, c_did, c_idx, e_did, e_idx_1)].append(0)
      collated[(hid, c_did, c_idx, e_did, e_idx_2)].append(1)
    else:
      collated[(hid, c_did, c_idx, e_did, e_idx_1)].append(1)
      collated[(hid, c_did, c_idx, e_did, e_idx_2)].append(0)
  
  out = []
  for task, votes in collated.items():
    hid, c_did, c_idx, e_did, e_idx = task
    score = sum(votes) / len(votes)
    if score == 0:
      score = -1
    claim = get_sentence(c_did, c_idx, document_path)
    evd = get_sentence(e_did, e_idx, document_path)
    out.append([hid, c_did, c_idx, e_did, claim, evd, score])

  return out

def aggregate_annotate_pointwise(index, document_path):
  """Generates pointwise annotations.

  Returns:
    [[] ...., claim, evd, score ]]
  """
  data = []

  for task_id, current in index.items():
    hid, cdi, idx, edi = task_id
    
    relevant = current['relevant']
    irrelevant = current['irrelevant']
    ignore = current['ignore']

    claim = get_sentence(cdi, idx, document_path)

    for r_idx in relevant:
      evd = get_sentence(edi, r_idx, document_path)
      data.append([hid, cdi, idx, edi, claim, evd, 1.])

    for ir_idx in random.sample(irrelevant,  min(len(relevant), len(irrelevant))):
      evd = get_sentence(edi, ir_idx, document_path)
      data.append([hid, cdi, idx, edi, claim, evd, -1.])

    for ig_idx in random.sample(ignore,  min(len(relevant), len(ignore))):
      evd = get_sentence(edi, ig_idx, document_path)
      data.append([hid, cdi, idx, edi, claim, evd, 0.])

  return data

def split_x_y(data):
  _, _, _, _, claim, evd, score = zip(*data)
  X = list(zip(claim, evd))
  return X, score

def main(config):
  data = pd.read_csv(config.in_path+"answers.csv")
  selected = data[["HITId", "Input.document_id", "Input.index", "Input.document_r_id", "Input.evidence_index_1", "Input.evidence_index_2", "Answer.choice"]]

  if config.aggregate:
    index = build_index(selected.values)
    filled_index = fill_index(index, config.document_path)
    annotated_data = aggregate_annotate_pointwise(filled_index, config.document_path)
  else:
    annotated_data = annotate_pointwise(selected.values, config.document_path)

  # stratify across documents to prevent data leakage.
  data_train, temp = stratify_split(annotated_data, test_size=0.30)
  data_test, data_val = stratify_split(temp, test_size=0.50)
  
  # split out data
  X_train, y_train = split_x_y(data_train)
  X_val, y_val = split_x_y(data_val)
  X_test, y_test = split_x_y(data_test)

  if config.verbose:
    assert len(X_train) == len(y_train)
    assert len(X_test) == len(y_test)
    assert len(X_val) == len(y_val)
    print("train: {}, test: {}, validation: {}".format(len(X_train), len(X_test), len(X_val)))

  # save files.
  save_file(X_train, y_train, 'train', data_path=config.out_path)
  save_file(X_test, y_test, 'test', data_path=config.out_path)
  save_file(X_val, y_val, 'val', data_path=config.out_path)

if __name__ == "__main__":

  parser = argparse.ArgumentParser()

  # utils
  parser.add_argument('--document_path', type=str, default="../data/")
  parser.add_argument('--in_path', type=str, default="../data/results/1/")
  parser.add_argument('--out_path', type=str, default="../data/results/1-pointwise-infer/")
  parser.add_argument('--verbose', type=bool, default=True)
  parser.add_argument('--aggregate', type=bool, default=False)

  config = parser.parse_args()

  # generate pointwise data
  main(config)