# -*- coding: utf-8 -*- 

import json
import argparse
import nltk

from tqdm import trange
from sentence_normalizer import SentenceNormalizer

def main(config):
  nltk.download('punkt')
  normalizer = SentenceNormalizer()
  with open(config.data_path+'raw/documents.json') as jf:
    document_list = json.load(jf)
  all_sentences = []
  for idx in trange(len(document_list)):
    _document = document_list[idx]
    with open(config.data_path+'documents/{}.json'.format(_document), 'r') as jf:
      document = json.load(jf)
    sentences = document['sentences']
    normalized_sentences = normalizer.fit_transform(sentences)
    document['sentences'] = normalized_sentences
    with open(config.data_path+'clean/documents/{}.json'.format(_document), 'w') as jf:
      json.dump(document, jf)
    all_sentences.extend(normalized_sentences)

  big = " ".join(all_sentences)
  with open(config.data_path+'clean/all.txt', 'w') as f:
    f.write(big)

  with open(config.data_path+'clean/sentences.txt', 'w') as f:
    f.writelines(all_sentences)

  vocab = list(set(big.split()))
  with open(config.data_path+'clean/vocab.json', 'w') as jf:
    json.dump(vocab, jf)

if __name__ == "__main__":
  parser = argparse.ArgumentParser()

  # io
  parser.add_argument('--data_path', type=str, default="../data/")

  # extract data and vote
  config = parser.parse_args()

  main(config)