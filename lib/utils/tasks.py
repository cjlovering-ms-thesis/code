import numpy as np
from tqdm import tqdm
import random

def join_tasks(tasks, gold):
  out = []
  g_idx = 0
  per_task_idx = 15 # assumes
  for t in tqdm(tasks, desc="Joining tasks."):
    g_1 = gold[g_idx]
    if g_idx + 1 >= len(gold):
      g_idx = 0
    else:
      g_idx += 1
    g_2 = gold[g_idx]
    places = list(reversed(sorted(list(np.random.choice(5, 2, replace=False)))))
    second, first = places
    assert first < second
    second, first = second * per_task_idx, first * per_task_idx
    t[second:second] = g_2
    t[first:first] = g_1
    out.append(t)
    if g_idx == 0:
      random.shuffle(gold)
      g_idx = -1
  return out
