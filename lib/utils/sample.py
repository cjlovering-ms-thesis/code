import itertools
import argparse
import random
from collections import defaultdict

def sample(l):
    return random.sample()

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('input', action="store",  help='path to a input csv file')
    parser.add_argument('output', action="store", help='path to a output dat file')
    parser.add_argument('--sample', nargs=1, help='k for voting', type=int)

    args = parser.parse_args()

    sample = args.sample[0]
    in_path = args.input
    out_path = args.output

    with open(in_path, 'r') as f:
        lines = f.readlines()
    
    data = lines[1:]
    data = [ [int(_d) for _d in d.split()] for d in data ]
    votes = defaultdict(list)
    for d in data:
        votes[d[0]].append((d[1], d[2]))

    out = { k:random.sample(v, sample) for k,v in votes.iteritems() }
    flat = []

    for k,l in out.iteritems():
        for a,b in l:
            flat.append((k,a,b))

    with open(out_path, 'w') as f:
        f.write("{}".format(lines[0]))
        for line in flat:
            k,a,b=line
            f.write("{} {} {}\n".format(k,a,b))
