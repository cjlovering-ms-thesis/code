import json
from collections import defaultdict
from extract import to_label

def build_index(data):
  """Structures data to capture positively ranked items.

  Args:
      data List[List[]]: List of results from MTURK.
  Returns:
      index (defaultdict({
          'relevant': set(),
          'ignore': set(),
          'irrelevant': set()
      })): Tracks relevant items, items that can be ignored,
          and items we can sample as irrelevant.
  """
  def get_dict():
    """Returns default structure for index items"""
    return {
      'relevant': set(),
      'ignore': set(),
      'irrelevant': set()
    }

  index = defaultdict(get_dict)

  for d in data:

    hid, cdi, idx, edi, e1_idx, e2_idx, label = d
    task_id = (hid, cdi, idx, edi)

    # if 1 is pos, move 1 to releveant and 2 to ignore, else vice versa.
    if to_label(label):
      index[task_id]['relevant'].add(e2_idx)
      index[task_id]['ignore'].add(e1_idx)
    else: 
      index[task_id]['relevant'].add(e1_idx)
      index[task_id]['ignore'].add(e2_idx)

  return index

def fill_index(index, document_path):
  """ NOTE: This is currently not `functional`... """
  def _add_to_irrelevant_huh(_current, _idx):
    return _idx not in _current['relevant'] and _idx not in _current['ignore']

  for task_id, current in index.items():
    hid, cdi, idx, edi = task_id
    irrelevant = set()
    sentences = get_document(edi, document_path)
    for _idx, _ in enumerate(sentences):
      if _add_to_irrelevant_huh(current, _idx):
        irrelevant.add(_idx)
    current['irrelevant'] = irrelevant

  return index

def get_document(document_id, document_path):
  """Fetches sentences of a document.

  Args:
      document_id (string): document id (and file name).

  Returns:
      (list(string)): List of all sentences in the document.
  """
  with open('{}clean/documents/{}.json'.format(document_path, document_id), 'r') as jf:
    document = json.load(jf)
  return document['sentences']
