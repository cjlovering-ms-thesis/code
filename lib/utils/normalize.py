from .sentence_normalizer import SentenceNormalizer

def normalize(sentences):
  """Normalizes sentences.
  """
  normalizer = SentenceNormalizer()
  normalized_sentences = normalizer.fit_transform(sentences)
  return normalized_sentences

