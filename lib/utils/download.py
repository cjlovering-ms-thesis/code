# -*- coding: utf-8 -*-

"""This downloads and converts all fetched publications."""

from __future__ import print_function

import json
import os.path
from progress.bar import ChargingBar

from converter import convert_external_pdf

def download_papers(verbose=True):
    """Downloads, converts, and trims papers from bibs. """
    
    # read all publications downloaded
    with open("data/raw/bib.json", 'r') as jf:
        pubs = json.load(jf)

    # load most-recent download results
    with open("data/raw/download_results.json", 'r') as jf:
        results= json.load(jf)
    
    # track results
    _skipped, _success, _download, _converted = 0, 0, 0, 0
    
    progress = ChargingBar('Downloading', max=len(pubs))

    # process all publications
    for i,p in enumerate(pubs):

        # increment progress
        progress.next()

        # stash results
        with open('data/raw/download_results.json', 'w') as jf:
            json.dump(results, jf, indent=1)

        # grab info
        link = p['bib']['eprint'].replace("http://scholar.google.com/","")
        p_id = p['id_scholarcitedby']

        # basic checks on link validity + no repeats
        if not _check_link(p_id, link, results):
            results[p_id] = {}
            print(">", end="")
            continue

        # attempt to download paper
        try:            
            dl, cv = download_external(link, p_id)
            results[p_id]['downloaded'] = dl
            results[p_id]['converted']  = cv
            if dl and cv:
                _success += 1
            if not dl:
                _download += 1
            if not cv:
                _converted += 1
        except:
            print("x", end="")
            results[p_id]['hard_fail'] = True
    
    # progress completed
    progress.finish()   
    
    # output statistics
    if verbose:
        print("Skipped {} downloads.".format(_skipped))
        print("Failed downloading {} publications".format(_download))
        print("Failed converting {} publications".format(_converted))
        print("Successfully downloaded {} publications".format(_success))

    with open('data/raw/download_bib_results.json', 'w') as jf:
        json.dump(results, jf, indent=1)

def _check_link(p_id, link, results):
    """Basic checks that the link is valid and NOT prev downloaded. """
    if p_id in results:
        return False
    if os.path.isfile('data/papers/pdf/{}.pdf'.format(p_id)):
        return False 
    if '/scholar' in link:
        results[p_id]['bad_link'] = "scholar"
        return False
    if "ENTRYTYPE" in p['bib'] and p['bib']["ENTRYTYPE"] != "article":
        results[p_id]['bad_link'] = p['bib']["ENTRYTYPE"]
        return False
    if "https://books.google.com" in link:
        results[p_id]['bad_link'] = "books.google.com"
        return False
    return True

def _download_external(link, paper_id):
    """Downloads, converts, and trims a paper. """
    return convert_external_pdf(link, paper_id)

if __name__ == "__main__":
    download_papers()