import json

"""Currently the file paths are incorrect.
"""

def collate_downloads():
    """ Track downloaded papers. """
    with open('raw_data/download_bib_results.json','r') as jf:
        downloads = json.load(jf)
    filtered = [ k for k,v in downloads.iteritems() if 'converted' in v and v['converted'] ]
    with open('raw_data/downloads.json','w') as jf:
        json.dump(filtered, jf, indent=1)

def collate_documents():
    """ Join documents. """
    with open('raw_data/corpus.json', 'r') as jf:
        corpus = json.load(jf)
    with open('raw_data/downloads.json', 'r') as jf:
        downloads = json.load(jf)
    documents = corpus + downloads
    with open('raw_data/documents.json','w') as jf:
        json.dump(documents, jf, indent=1)

if __name__ == "__main__":
    collate_downloads()
    collate_documents()
