from typing import List, Dict, Any, Tuple

def read_tasks(tasks: List[Dict[str, Any]]) -> Dict[Tuple, str]:
    out = {}
    for task in tasks:
        document_id = task['document_id']
        index = task['index']
        reference_id = task['reference_id']
        document_r_id = task['document_r_id']
        evidence_index_1 = task['evidence_index_1']
        evidence_index_2 = task['evidence_index_2']
        task_index = task['task_index']
        out[(document_id, index, reference_id, document_r_id, evidence_index_1, evidence_index_2)] = task_index
    return out
    
def write_tasks(tasks:  Dict[Tuple, str]) -> List[Dict[str, Any]]:
    out = []
    for k,v in tasks.iteritems():
        a, b, c, d, e, f = k
        out.append({
            'document_id': a,
            'index': b,
            'reference_id': c,
            'document_r_id': d,
            'evidence_index_1': e,
            'evidence_index_2': f,
            'task_index': v
        })
    return out

def to_label(keyword: str) -> int:
    """ Converts string into label given a target keyword
    
        Args:
            classification - a given string of an annotation
            keyword - target classification (e.g. 'related')
        Returns:
            1 if match, 0 if different
    """
    if "optionA" == keyword:
        return 1
    return 0