
import json
import numpy as np

from tqdm import tqdm
from typing import List

from . import clean
from . import documents

def get_claims(claims, doc_list, ref_bib_map, docs) -> List:
  valid_claims = []
  for c in tqdm(claims, desc="Selecting claims"):
    if c['document_id'] not in doc_list:
      # claim document not tracked.
      continue
    for r in c['references']:
      # find and save exactly 1 referencing paper per document
      if r in ref_bib_map and ref_bib_map[r] in docs:
        valid_claims.append(c)
  return valid_claims

def bundle_data(config, claims, ref_bib_map, docs):
  data = [] # { claim sentence, a ref sentence }
  for c in tqdm(claims, desc="Bundling data"):
    claim_sentence = docs[c['document_id']]['sentences'][c['index']]
    if c['index'] >= config.max_embed_sentences:
      continue
    for r in c['references']:
      if r in ref_bib_map and ref_bib_map[r] in docs:
        referencing_sentences = docs[ref_bib_map[r]]['sentences']
        data.append({
          'claim': claim_sentence,
          'sentences': [ rs for rs in referencing_sentences ][:config.max_embed_sentences],
          'document_r_id': ref_bib_map[r],
          'document_id': c['document_id'],
          'index': c['index'],
          'reference_id': r
        })
  return data

def load_data(config):
  """Loads meta data from disk. """
  with open(config.data_path+'raw/documents.json', 'r', encoding='utf-8') as jf:
    doc_list = json.load(jf)
  with open(config.data_path+'raw/claims.json', 'r', encoding='utf-8') as jf:
    claims = json.load(jf)
  with open(config.data_path+'raw/ref_bib_map.json', 'r', encoding='utf-8') as jf:
    ref_bib_map = json.load(jf)
  return doc_list, claims, ref_bib_map

def get_claim_data(config):
  
  # load data
  doc_list, claims, ref_bib_map = load_data(config)

  # load all the documents
  docs = documents.load_documents(config.data_path, doc_list)

  # find valid claims
  claims = get_claims(claims, doc_list, ref_bib_map, docs)

  # bundle data into claim, evidence, errata tuples
  data = bundle_data(config, claims, ref_bib_map, docs)

  # filter data for *good* datums
  data = [datum for datum in data if clean.datum_valid(datum)]

  return data