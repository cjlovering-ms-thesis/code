import json
import argparse

from documents import save_document_file, save_file
from extract import balance_data, extract_x_y

def load(path="dataset.json"):
  with open(path, 'r') as jf:
    data = json.load(jf)
  return data

def split(data):
  train = [d for d in data if d['_split'] == 'train']
  test = [d for d in data if d['_split'] == 'test']
  return train, test

def to_document_data(split_data):
  def init():
    return {
      'X': [],
      'y': [],
      'claim': ""
    }
  out = []
  for _d in split_data:
    _d = _d['_annotation']
    for d in _d:
      doc = init()
      doc['claim'] = d['_citing_sentence']
      doc['y'] = d['_labeled_lineIDs']
      doc['X'] = d['_doc_body']
      out.append(doc)
  return out

def to_ranking(data):
  out = []
  for doc in data:
    new_doc = {**doc}
    total = len(doc['X'])
    y_set = set(doc['y'])
    new_y = []
    for idx in range(total):
      if idx in y_set:
        continue
      for idx_2 in y_set:
        new_y.append([idx, idx_2, 1])

    # add placeholder claim.
    tampered_new_y = [ ["", *y] for y in new_y ]
    # balance data.
    balanced_new_x, balanced_new_y = balance_data(*extract_x_y(tampered_new_y))
    # zip together the x and y, and then flatten it.
    zipped_new_y = [[*z[0], z[1]] for z in zip(balanced_new_x, balanced_new_y)]
    # remove the placeholder claim.
    final_new_y = [z[1:] for z in zipped_new_y]
    # save the results.
    new_doc['y'] = final_new_y
    out.append(new_doc)
  return out

def to_2_class(data):
  out = []
  for doc in data:
    new_doc = {**doc}
    total = len(doc['X'])
    y_set = set(doc['y'])
    new_y = []
    for idx in range(total):
      if idx in y_set:
        new_y.append([ idx, 1 ])
      else:
        new_y.append([ idx, 0 ])
    new_doc['y'] = new_y
    out.append(new_doc)
  return out

def to_pairwise(data):
  data = to_ranking(data)
  out = []
  for doc in data:
    ys = doc['y']
    claim = doc['claim']
    for y in ys:
      idx_1, idx_2, label = y
      e_1, e_2 = doc['X'][idx_1], doc['X'][idx_2]
      out.append([claim, e_1, e_2, label])
  return out

def main(config):
  """Parse Argument dataset into our format. """
  data = load(config.in_path)
  train, test = split(data)
  train_data, test_data = to_document_data(train), to_document_data(test)

  # balance pairwise labels.
  train_data_pair_rank, test_data_pair_rank = to_pairwise(train_data), to_pairwise(train_data)
  train_data_pair_rank_X, train_data_pair_rank_y = balance_data(*extract_x_y(train_data_pair_rank))
  test_data_pair_rank_X, test_data_pair_rank_y = balance_data(*extract_x_y(test_data_pair_rank))

  # save pairwise label data files.
  save_file(train_data_pair_rank_X, train_data_pair_rank_y, 'train', config.out_path)
  save_file(test_data_pair_rank_X, test_data_pair_rank_y, 'test', config.out_path)
  save_file(test_data_pair_rank_X, test_data_pair_rank_y, 'val', config.out_path)

  # save document level files
  train_data_rank, test_data_rank = to_ranking(train_data), to_ranking(train_data)

  save_document_file(train_data_rank, 'train', config.out_path)
  save_document_file(test_data_rank, 'test', config.out_path)
  save_document_file(test_data_rank, 'val', config.out_path)
  # we are not hyper-param optimizing on this DS - it's just makes it easier to load the data.
  # (the authors did not provide a validation split).


if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  # io
  parser.add_argument('--in_path', type=str, default="../data/argument/dataset.json")
  parser.add_argument('--out_path', type=str, default="../data/results/argument/")

  # extract data and vote
  config = parser.parse_args()

  main(config)
