import numpy as np
import pandas as pd

from tqdm import tqdm
import csv

from . import claim_data
from . import encode
from . import multiple_choice
from . import pre_annotation_config

def create_gold_questions(data, config, gold_questions_to_create):
  """Creates gold standard questions from data.

  Note: This is an outline of the question. Manual work must be done to label it.
  """
  out = []
  CLAIM_IDX = 4
  QUESTION_IDX = 5
  PLACEHOLDER = "REPLACE THIS TEXT WITH EVIDENCE."
  for d in data:
    sentence_size = len(d['sentences'])
    if sentence_size < 4: continue
    random_claim_idx = np.random.randint(0, len(data))
    answer_idx = np.random.randint(0, 4)
    question = np.random.random_integers(0, sentence_size-1, 4)
    q = multiple_choice.format_question(d, [question], config, True, answer_idx)[0]
    claim = data[random_claim_idx]['claim']
    q[CLAIM_IDX] = claim
    q[QUESTION_IDX+answer_idx] = PLACEHOLDER
    out.append(q)
  sample = np.random.choice(len(out), gold_questions_to_create, False)
  return [out[idx] for idx in sample]

def main(config):

  # get data
  data = claim_data.get_claim_data(config)

  # gold standard
  gold_questions = create_gold_questions(data, config, config.gold_questions_to_create)

  # saving tasks
  out = [encode.encode_task(t) for t in tqdm(gold_questions, desc="Saving gold questions")]
  with open(config.data_path+'tasks/gold_questions_empty.csv', 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerows(out)
  pd.DataFrame(out).to_csv(config.data_path+'tasks/gold_questions_empty_pd.csv', index=False, header=False, sep=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)

if __name__ == "__main__":
  config = pre_annotation_config.get_config()
  main(config)