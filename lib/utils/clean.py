from typing import List

def clean(X, y):
  """Remove sentences that are really long or really short.
  """
  y_out = []
  for i in range(len(y)):
    c, e1, e2 = X[i]
    if dirty(c) or dirty(e1) or dirty(e2):
      y_out.append(None)
    else:
      y_out.append(y[i])
  return (
    [X[i]     for i,label in enumerate(y_out) if label is not None],
    [y_out[i] for i,label in enumerate(y_out) if label is not None]
  )

def dirty(s):
  checks = [
    lambda s: len(s) > 500,
    lambda s: len(s) < 22,
    lambda s: len(s.split(" ")) < 5,
    lambda s: len(s.split(" ")) > 250,
  ]
  return any([check(s) for check in checks])

def datum_valid(datum):
  if len(datum['sentences']) <= 5: return False
  if dirty(datum['claim']): return False
  filter_out = [
    "with permission of the copyright owner.",
    "by any means",
    "e.g.", # ends sentences sometimes
    "Please cite this article as",
    "Further reproduction prohibited without permission",
    "No part of this book may be reproduced or utilized in any",
    "views expressed herein do not necessarily represent",
    "please contact",
    "funded under an agreement",
    ".......",
    "?", # these more often than not are sentences which at best convery a point of view; they don't have evidence.
    "report is available",
    "How ",
  ]
  for s in datum['sentences']:
    for f in filter_out:
      if f in s:
        return False
  return True

def filter_evidence_idx(datum, idx: List[int]) -> List[int]:
  return [i for i in idx if not dirty(datum['sentences'][i])]
