from __future__ import print_function

import argparse
import h5py
import json
import torch

import numpy as np

from collections import defaultdict
from itertools import combinations
from tqdm import tqdm
from unidecode import unidecode

from pipeline import load_data, load_documents

def get_model(verbose: bool=True):
  """Configure and load InferSent """
  if verbose: print("Loading model...")
  model = torch.load(config.resource_path+'infersent.allnli.pickle', map_location=lambda storage, loc: storage)
  model.set_glove_path(config.glove_path)
  model.build_vocab_k_words(K=100000)
  return model

def embed_corpus(model, docs, config):
  blah, idx = 1026, 0
  for (d_id, doc) in tqdm(docs.items()):
    if idx < blah:
      idx += 1
      continue
    sentences = doc['sentences'][:config.max_embed_sentences]
    if len(sentences) < 1:
      embeddings = np.zeros((1, 4096))
    else:
      embeddings = model.encode(sentences, bsize=128, tokenize=True, verbose=False)
    with h5py.File(config.embedding_path+"embeddings/{}.h5".format(d_id), 'w') as hf:
      hf.create_dataset(d_id, data=embeddings)

def main(config):
  # load model
  model = get_model(config.verbose)

  # load data
  doc_list, _, _ = load_data(config)

  # load all the documents
  docs = load_documents(config.data_path, doc_list)

  # embed corpus
  embed_corpus(model, docs, config)


if __name__ == "__main__":
  parser = argparse.ArgumentParser()

  # misc
  parser.add_argument('--verbose', type=bool, default=True)

  # io
  parser.add_argument('--glove_path', type=str, default="../resources/glove.840B.300d.txt")
  parser.add_argument('--resource_path', type=str, default="../resources/")
  parser.add_argument('--data_path', type=str, default="../data/")
  parser.add_argument('--embedding_path', type=str, default="../data/")

  # settings
  parser.add_argument('--max_embed_sentences', type=int, default=250)

  # extract data and vote
  config = parser.parse_args()

  main(config)