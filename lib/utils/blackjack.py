from typing import Any, Callable, List

BIG_VAL = 2e100

def blackjack(items: List[Any], target: int, item_size: Callable=lambda x:len(x)):
  """Naive knapsack with no weights (i.e. blackjack)

  Args:
    item (List[Any]): List of any item.

  """
  def rec(idx: int, count: int, kept=[]):
    if count > target:
      return BIG_VAL, kept
    if idx == len(items):
      return target - count, kept
    left, kept_left = rec(idx+1, count, kept)
    right, kept_right = rec(idx+1, count+item_size(items[idx]), kept+[idx])
    if left < right:
      return left, kept_left
    return right, kept_right
  _, kept = rec(0, 0)
  return [ items[idx] for idx in kept ]
