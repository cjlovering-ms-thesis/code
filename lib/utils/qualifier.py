import numpy as np
import pandas as pd
import itertools
from tqdm import tqdm
import csv

from . import claim_data
from . import create_dataset
from . import encode
from . import pre_annotation_config

def grade_qualifier(in_path: str, out_path: str):
  data_in = pd.read_csv(in_path)
  data_out = {'WorkerId': [], 'Total': [], 'Cohen': [], 'AUC': [], 'GoldTotal': [], 'GoldCohen': [], 'GoldAUC': []}
  df_out = pd.DataFrame(data=data_out)
  df_out.to_csv(out_path)

def main(config):
  gold = pd.read_csv(config.data_path+'tasks/gold_questions_full.csv', sep=",", quotechar='"', quoting=csv.QUOTE_MINIMAL, header=None).values
  hard = pd.read_csv(config.data_path+'tasks/hard_questions_full.csv', sep=",", quotechar='"', quoting=csv.QUOTE_MINIMAL, header=None).values
  join = [list(itertools.chain(*(list(hard) + list(gold)[:4])))]
  out = [encode.encode_task(t) for t in tqdm(join, desc="Saving gold questions")]
  with open(config.data_path+'tasks/qualifier.csv', 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(create_dataset.header(6, 4, 4))
    writer.writerows(out)

if __name__ == "__main__":
  config = pre_annotation_config.get_config()
  main(config)