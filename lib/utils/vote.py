""" Expects file name in args. Expects normalized format.
"""

import argparse
import numpy as np
import random
from collections import defaultdict

def parse(line):
    return [ int(l) for l in line.split() ]

def vote(v):
    N = len(v)
    P = np.sum(v) * 1.0
    if P / N > 0.5:
        return 1
    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('input', action="store",  help='path to a input csv file')
    parser.add_argument('output', action="store", help='path to a output dat file')
    args = parser.parse_args()
    in_path = args.input
    out_path = args.output
    with open(in_path, 'r') as f:
        lines = f.readlines()
    
    lines = lines[1:]
    data = [ parse(line) for line in lines ]
    votes = defaultdict(list)
    for d in data:
        votes[d[0]].append(d[2])
    out = { k:vote(v) for k,v in votes.iteritems() }
    with open(out_path, 'w') as f:
        f.write("task,vote\n")
        for k,v in out.iteritems():
            f.write("{},{}\n".format(k,v))



