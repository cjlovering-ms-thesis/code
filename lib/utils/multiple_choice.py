def format_question(d, questions, config, is_gold: bool=False, answer_idx: int=-1):
  """Flattens a question's data into a list.
  """
  document_id = d['document_id']
  document_r_id = d['document_r_id']
  index = d['index']
  reference_id = d['reference_id']
  sentences = d['sentences']
  claim = d['claim']
  out = []
  for question in questions:
    task = []
    remaining = [document_id, index, reference_id, document_r_id]
    task.extend(remaining)
    task.append(claim)
    task.extend([sentences[mc_idx] for mc_idx in question])
    task.extend([mc_idx for mc_idx in question])
    task.append(is_gold)
    task.append(answer_idx)
    out.append(task)
  return out