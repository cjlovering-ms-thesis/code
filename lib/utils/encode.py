import csv
import re

def limit_to_BMP(value, pattern):
  """ SF: https://stackoverflow.com/questions/25362251/python-process-a-csv-file-to-remove-unicode-characters-greater-than-3-bytes """
  return pattern.sub(u'\uFFFD', value).encode('utf8')


def encode_sentence(s, pattern):
  """Encodes input if its a string. """
  if type(s) != str:
    return s
  # .encode('ascii','ignore').decode()
  # print(s.replace('\n','').replace('\'', '')
  return limit_to_BMP(s.replace('\n','').replace('\'', '').replace("\t\r", " ").replace("\t", " ").replace("  ", " ").encode("ascii", "ignore").decode(), pattern)

def encode_task(t):
  """Encodes every sentences into utf-8. """
  re_pattern = re.compile(u'[^\u0000-\uD7FF\uE000-\uFFFF]', re.UNICODE)
  return [ encode_sentence(_p, re_pattern) for _p in t ]