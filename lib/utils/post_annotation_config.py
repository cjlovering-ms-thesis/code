import argparse

def get_config():
  parser = argparse.ArgumentParser()

  # misc
  parser.add_argument('--verbose', type=bool, default=True)

  # io
  parser.add_argument('--glove_path', type=str, default="../resources/glove.840B.300d.txt")
  parser.add_argument('--resource_path', type=str, default="../resources/")
  parser.add_argument('--data_path', type=str, default="../data/")
  parser.add_argument('--embedding_path', type=str, default="../data/")

  # settings
  parser.add_argument('--k_top_similar', type=int, default=25)
  """
  When multiple_choice_options is 4, then we get 3 labels per question.
  We want n log n (n = k_top_similar) labels. The combinations sampled is set
  to get the correct value. We could instead calc it here, but this should
  be simpler if we don't change these values often.
  """
  parser.add_argument('--sets_needed', type=int, default=3)
  parser.add_argument('--multiple_choice_options', type=int, default=4)
  parser.add_argument('--questions_per_task', type=int, default=8)
  parser.add_argument('--gold_per_task', type=int, default=2)
  parser.add_argument('--max_embed_sentences', type=int, default=250)

  # gold standard options
  parser.add_argument('--gold_questions_to_create', type=int, default=50)

  # extract data and vote
  config = parser.parse_args()

  return config