import argparse
import json
import pandas as pd
from tqdm import trange

from utils import read_tasks, write_tasks, to_label

"""
Input.document_id, Input.index, Input.reference_id, Input.document_r_id, Input.evidence_index_1, Input.evidence_index_2, Answer.choice
"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('input',  action="store",  help='path to a input csv file')
    parser.add_argument('output', action="store",  help='path to a output dat file')
    parser.add_argument('--tasks', nargs='?', help='optional path to a task file')
    parser.add_argument('--workers', nargs='?',help='optional path to a workers file')
    args = parser.parse_args()
    in_path, out_path = args.input, args.output    
    
    data = pd.read_csv(in_path)
    selected = data[["Input.document_id", "Input.index", "Input.reference_id", "Input.document_r_id", "Input.evidence_index_1", "Input.evidence_index_2", "Answer.choice"]]

    tasks = {}
    workers = {}
    i, i_init = 0, 0
    j, j_init = 0, 0

    if args.workers:
        with open(args.workers, 'r') as jf:
            workers = json.load(jf)
            i_init = max([w for w in workers.values()] + [-1])
            i = i_init + 1
    if args.tasks:
        with open(args.tasks, 'r') as jf:
            tasks = read_tasks(json.load(jf))
            j_init = max([t for t in tasks.values()] + [-1])
            j = j_init + 1
    rows = []
    for z in trange(len(data)):

        worker = data.loc[z, "WorkerId"]
        if worker not in workers:
            workers[worker] = i
            i += 1
        _worker = workers[worker]

        task = tuple([ str(s) for s in selected.loc[z].values[:-1].tolist() ])
        if task not in tasks:
            tasks[task] = j
            j += 1
        _task = tasks[task]
        
        label = to_label(data.loc[z, "Answer.choice"])
        rows.append((_task, _worker, label))

    number_labels = len(data)
    number_workers = i
    number_tasks = j

    with open(out_path, 'w') as f:
        f.write("{} {} {} {} 0.5 0.5\n".format(number_labels, number_workers, number_tasks, 2))
        for t, w, l in rows:
            f.write("{} {} {}\n".format(t, w, l))

    if args.tasks:
        with open(args.tasks, 'w') as jf:
            json.dump(write_tasks(tasks), jf)

    if args.workers:
        with open(args.workers, 'w') as jf:
            json.dump(workers, jf)
    
