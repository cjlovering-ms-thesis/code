# Utils

This file contains files relevant to this project in an ancillary manner.

For most important files there is a corresponding .sh script in `/scripts`.

A few of the files are used (were used) primarily for generating plain labels
for glad and aggregation. We don't currently use this, but it was appropriate
for the initial comparison.

Now, we use `process.sh` to process and extract training data.
