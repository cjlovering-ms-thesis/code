import h5py
import json

from typing import Dict, List
from tqdm import tqdm

def load_documents(data_path: str, doc_list) -> Dict:
  """Loads documents as json from file. """
  docs = {}
  for d_id in tqdm(doc_list, desc="Loading documents"):
    with open(data_path+'documents/{0}.json'.format(d_id), 'r') as jf:
      docs[d_id] = json.load(jf)
  return docs

def get_document_sentences(document_id: str, document_path: str) -> List:
  """Gets sentences from a given file.

  Args:
      document_id (str): Id of a document id.
      sen_index (int): The index of a sentence.
  Returns:
      (str): The target sentence.
  """
  with open(document_path+'clean/documents/{}.json'.format(document_id), 'r') as jf:
    document = json.load(jf)
  sentences = document['sentences']
  return sentences

def get_sentence(document_id: str, sen_index: int, document_path: str) -> str:
  """Gets sentences from a given file.

  Args:
      document_id (str): Id of a document id.
      sen_index (int): The index of a sentence.
  Returns:
      (str): The target sentence.
  """
  with open(document_path+'clean/documents/{}.json'.format(document_id), 'r') as jf:
    document = json.load(jf)
    sentences = document['sentences']
    return sentences[sen_index]

def save_file(X: List, y: List, name: str, data_path: str) -> None:
  """Saves files for X and y data. """
  with open("{}X_{}.json".format(data_path, name), 'w') as jf:
    json.dump(X, jf)
  with open("{}y_{}.json".format(data_path, name), 'w') as jf:
    json.dump(y, jf)

def save_document_file(documents: List[Dict], name: str, data_path: str) -> None:
  """Saves files for document-version data. """
  with open("{}document_{}.json".format(data_path, name), 'w') as jf:
    json.dump(documents, jf)

def get_document_embedding(document_id: str, document_path: str) -> List:
  """Gets sentences from a given file.
  Args:
      document_id (str): Id of a document id.
      sen_index (int): The index of a sentence.
  Returns:
      (str): The target sentence.
  """
  dataset = h5py.File("{}/embeddings/{}.h5".format(document_path, document_id), 'r')
  return dataset[document_id][:]

def get_embedding(document_id: str, sentence_idx: int, document_path: str) -> str:
  """Gets sentences from a given file.

  Args:
      document_id (str): Id of a document id.
      sentence_idx (int): The index of a sentence.
  Returns:
      (str): The target sentence.
  """
  embedding = get_document_embedding(document_id, document_path)
  return embedding[sentence_idx]
