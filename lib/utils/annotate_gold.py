import argparse
import pandas as pd

def annotate_gold_questions(in_path: str, out_path: str, questions: int=10, whitelist_path: str="") -> None:
  """Annotates results at in_path and writes out data to out_path. 
  """
  REJECT_MESSAGE = 'Unfortunately, too many of the answers you submitted for this HIT are incorrect. As noted in the instructions, we require a sufficiently high accuracy in order to accept your work.'
  data_in = pd.read_csv(in_path)
  data_out = data_in.copy()
  num_data = data_in.shape[0]
  for q_idx in range(questions):
    for d_idx in range(num_data):
      # If Q is a gold std., and answered incorrectly - reject the hit.
      if data_in.loc[d_idx, "Input.is_gold_{}".format(str(q_idx))] \
        and data_in.loc[d_idx, "Input.gold_answer_{}".format(str(q_idx))] \
            != data_in.loc[d_idx, "Answer.e_{}".format(str(q_idx))]:
        data_out.loc[d_idx, 'Reject'] = REJECT_MESSAGE

  # For rows which did not reject (for whatever reason), accept the HIT.
  for d_idx in range(num_data):
    if pd.isnull(data_out.loc[d_idx, 'Reject']):
      data_out.loc[d_idx, 'Approve'] = 'x'
  
  # USE THIS WHEN UPLOADING, NOT ANALYZING.
  if whitelist_path:
    data_out = apply_whitelist(data_out, whitelist_path)

  # save results.
  data_out.to_csv(out_path)

def apply_whitelist(data, path):
  whitelist = pd.read_csv(path)
  for hitid, workerid in whitelist.values:
    data.loc[ (data['HITId'] == hitid) & (data['WorkerId'] == workerid), 'Reject'] = ""
    data.loc[ (data['HITId'] == hitid) & (data['WorkerId'] == workerid), 'Approve'] = "x"
  return data

def main(config):
  annotate_gold_questions(config.in_path, config.out_path, config.questions, config.whitelist_path)

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('--questions', type=int, default=10)
  parser.add_argument('--whitelist_path', type=str, default="")
  parser.add_argument('--in_path', type=str, default="../data/output/results.csv")
  parser.add_argument('--out_path', type=str, default="../data/output/annotated.csv")
  config = parser.parse_args()

  main(config)