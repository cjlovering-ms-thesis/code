from __future__ import print_function

import numpy as np
import json
import csv
import json
import uuid
import random
import math
import argparse

import pandas as pd
import matplotlib.pyplot as plt

from collections import Counter, defaultdict
from itertools import chain
from sklearn.model_selection import train_test_split
from typing import Dict, Any, Tuple, List

from .documents import get_document_sentences, get_sentence, save_file, save_document_file
from .blackjack import blackjack

def to_label(answer: str) -> int:
  """Converts string into label given a target keyword

  Args:
      classification (string): The label annotating a pairing.
  Returns:
      0 if match, 1 otherwise.
  """
  if "optionA" == answer:
    return 0
  return 1

def vote(v: List, vote_config: Dict[str, bool]) -> int:
  """Vote on a set of MTURK responses.

  Args:
      v (list): list of responses. the last item in a response is the label.
  Returns: 
      1 if majority voted positively, 0 otherwise.

  Note:: Predicated on the number of total votes being 5.
  """

  if len(v) != 5:
    raise Exception("Number of votes {} unexpected.".format(len(v)))

  v = [_d[-1] for _d in v]
  N = len(v)
  P = np.sum(v) * 1.0
  
  if vote_config.unanimous:
    if P == 5:
      return 1
    if P == 0:
      return 0
    return None
  if vote_config.high_majority:
    if P >= 4:
      return 1
    if P <= 1:
      return 0
    return None
  if vote_config.exact_majority:
    if P == 3:
      return 1
    if P == 2:
      return 0
  if vote_config.exact_high_majority:
    if P == 4:
      return 1
    if P == 1:
      return 0
  if vote_config.majority:
    if P >= 3:
      return 1
    return 0
  return None

def transform_data(data: List, vote_config: Dict[str, bool]) -> List:
  """Transforms data into voted data.

  Args:
      data (List): All the responses from MTURK.

  Returns:
      voted (list): Same format as input data, but 1/5 as much.
  """
  out = defaultdict(list)
  # group data
  for d in data:
    # get HIT id and the answer
    hid, answer = d[0], d[-1]
    # convert answer to number
    label = to_label(answer)
    # copy data
    updated = [*d]
    # insert new label
    updated[-1] = label
    # group annotated distance to other voted instances for HIT.
    out[hid].append(updated)

  # vote data
  voted = []
  for dl in out.values():
    # vote on list of hits for that vote
    label = vote(dl, vote_config)
    # copy data information
    updated = [*dl[0]]
    # overwrite original answer
    updated[-1] = label
    # save answer
    voted.append(updated)

  # filter data that was not confident enough, and marked None.
  confident = [ datum for datum in voted if datum[-1] is not None ]
  return confident

def extract_x_y(data: List) -> Tuple[List, List]:
  """Splits data into X and y. """
  X = [ d[:-1] for  d in data ]
  y = [ d[-1] for d in data ]
  return X, y

def fetch_data(data: List, document_path: str) -> List:
  """Fetches and formats data into sentences, not indexes.

  Args:
      data (list): All the responses from MTURK.
  Returns:
      data (list): Data formatted into sentences.
  """
  out = []
  for d in data:
    hid, cdi, idx, edi, e1_idx, e2_idx, label = d
    c = get_sentence(cdi, idx, document_path)
    s1 = get_sentence(edi, e1_idx, document_path)
    s2 = get_sentence(edi, e2_idx, document_path)
    out.append([c, s1, s2, label])
  return out

def stratify_split(data: List, test_size: float=0.30, random_state: int=42):
  """Stratifies data into splits, not permitting any data leakage. 
  
  Args:
      data (List): Raw format from MTURK files.
        hid, cdi, idx, edi, e1_idx, e2_idx, label
      test_size (float): Minimum percentage of data to be set to test.
      random_state (int): Seed set of randomness.
  Returns:
      train (List): Train data.
      test (list): Test data.

  Predicated on the fact that the data comes in an ordered fashion with all items
  in a task being consecutive.
  """
  # target size of test.
  target = int(math.ceil(len(data) * (1.0-test_size)))
  # find chunks within documents.
  chunk_lists = chunkify(data)
  # find an optimal partition
  partition = blackjack(chunk_lists, target)
  # flatten idx of chunks
  partition_idx = list(chain.from_iterable(partition))
  # select partition data items
  target_data = [data[idx] for idx in partition_idx]
  # select other data based on the difference of the sets.
  target_idx_set = set(partition_idx)
  other_data  = [data[idx] for idx in range(len(data)) if idx not in target_idx_set]
  # return other_data, target_data because target is blackjacked.
  return target_data, other_data

def chunkify(data: List) -> List:
  # set of all id information that cannot be leaked between sets.
  contaminated = set()
  # set of data idx which have been added to the target set.
  placed = set()

  order = [i for i in range(len(data))]
  chunks = []

  # find chunks of connected documents.
  for i in order:
    chunk = set()
    if i in placed:
      # if this is contaminated, we've already tracked it, thus its in placed.
      continue

    # all identifying pieces of information are contaminated.
    a, b, c = data[i][0], data[i][1], data[i][3]
    contaminated.add(a)
    contaminated.add(b)
    contaminated.add(c)

    # track the item in this chunk.
    chunk.add(i)
    placed.add(i)

    # track all the items for each of these identifying pieces of information.
    stack = [a, b, c]
    
    # find complete cut of touching items
    while stack:
      z = stack.pop()
      for j in order:
        if j in placed:
          # j is i or otherwise j is already placed.
          continue
        ja, jb, jc = data[j][0], data[j][1], data[j][3]
        if ja == z or jb == z or jc == z:
          placed.add(j)
          chunk.add(j)
          if ja not in contaminated:
            contaminated.add(ja)
            stack.append(ja)
          if jb not in contaminated:
            contaminated.add(jb)
            stack.append(jb)
          if jc not in contaminated:
            contaminated.add(jc)
            stack.append(jc)
    chunks.append(chunk)

  # convert sets to lists
  chunk_lists = [list(chunk) for chunk in chunks]
  return chunk_lists

def format_data(data: List, document_path: str="") -> Tuple[List, List]:
  """ Format data and return split of X, y. """
  fetched = fetch_data(data, document_path)
  X, y = extract_x_y(fetched)
  return X, y

def balance_data(X: List, y: List) -> Tuple[List, List]:
  """Balance the number of positive and negative votes, i.e. left and right.

  Note:
    Because the order of the evidence is arbitrary (and as of now) has no affect
    on the computation and no semantic meaning, we can flip both the label and the
    order to make it easier to interpret the results.
  """
  mid = len(y) // 2
  total = len(y)

  X_out = []
  y_out = []

  for i in range(mid):
    c, e1, e2 = X[i]
    if y[i] == 0:
      X_out.append([c, e1, e2])
    if y[i] == 1:
      X_out.append([c, e2, e1])
    y_out.append(0)
  for i in range(mid, total):
    c, e1, e2 = X[i]
    if y[i] == 1:
      X_out.append([c, e1, e2])
    if y[i] == 0:
      X_out.append([c, e2, e1])
    y_out.append(1)
  
  def shuffle(l, p):
    return [l[idx] for idx in p]
  shuffled = np.random.permutation(total)
  return shuffle(X_out, shuffled), shuffle(y_out, shuffled)

def clean(X, y):
  """Remove sentences that are really long or really short.
  """
  def dirty(s):
    checks = [
      lambda s: len(s) < 10,
      lambda s: len(s.split(" ")) < 5,
    ]
    return any([check(s) for check in checks])
  y_out = []
  for i in range(len(y)):
    c, e1, e2 = X[i]
    if dirty(c) or dirty(e1) or dirty(e2):
      y_out.append(None)
    else:
      y_out.append(y[i])
  return (
    [X[i]     for i,label in enumerate(y_out) if label is not None],
    [y_out[i] for i,label in enumerate(y_out) if label is not None]
  )

def correct(data: List) -> None:
  """Check that the data was split correctly. """
  v = [d[0] for d in data]
  c = Counter(v)
  l = len([g for g in c.values() if g is not 5])
  assert l == 0

def join_documents(data: List, document_path: str) -> List[Dict[str, Any]]:
  def init(): return {'claim':'', 'X':[], 'y':[]}
  join = defaultdict(init)

  for d in data:
    hid, cdi, idx, edi, e1_idx, e2_idx, label = d

    c = get_sentence(cdi, idx, document_path)

    join[(cdi, idx, edi)]['claim'] = c
    join[(cdi, idx, edi)]['y'].append( [e1_idx, e2_idx, label] )

  mutating_items = []
  for key,item in join.items():
    _, _, edi = key
    X = get_document_sentences(edi, document_path)
    item['X'] = X
    mutating_items.append(item)

  return mutating_items

"""
Input.document_id, Input.index, Input.reference_id, Input.document_r_id, Input.evidence_index_1, Input.evidence_index_2, Answer.choice
"""

def main(config: Dict[str, Any]) -> None: 
  # load data.
  data = pd.read_csv(config.in_path+"answers.csv")
  selected = data[["HITId", "Input.document_id", "Input.index", "Input.document_r_id", "Input.evidence_index_1", "Input.evidence_index_2", "Answer.choice"]]

  # vote on the data
  # NOTE: This is necessary to do before stratifying to keep balance.
  labeled = transform_data(selected.values, config)

  # stratify across documents to prevent data leakage.
  data_train, temp = stratify_split(labeled, test_size=0.30)
  data_val, data_test = stratify_split(temp, test_size=0.50)

  # document representation?
  if config.document_representation:
    print("train documents: {}".format(len(data_train)))
    print("val documents: {}".format(len(data_val)))
    print("test documents: {}".format(len(data_test)))

    train_documents = join_documents(data_train, document_path=config.document_path)
    val_documents   = join_documents(data_val, document_path=config.document_path)
    test_documents  = join_documents(data_test, document_path=config.document_path)
    
    print("train documents: {}".format(len(train_documents)))
    print("val documents: {}".format(len(val_documents)))
    print("test documents: {}".format(len(test_documents)))

    save_document_file(train_documents, 'train', config.out_path)
    save_document_file(val_documents, 'val', config.out_path)
    save_document_file(test_documents, 'test', config.out_path)

    return train_documents, val_documents, test_documents

  # format data.
  X_train, y_train = format_data(data_train, document_path=config.document_path)
  X_val, y_val = format_data(data_val, document_path=config.document_path)
  X_test, y_test = format_data(data_test, document_path=config.document_path)

  if config.clean:
    X_train, y_train = clean(X_train, y_train)
    X_val, y_val = clean(X_val, y_val)
    X_test, y_test = clean(X_test, y_test)

  if config.verbose:
    assert len(X_train) == len(y_train)
    assert len(X_test) == len(y_test)
    assert len(X_val) == len(y_val)
    print("total: {}, train: {}, test: {}, validation: {}".format(len(X_train) + len(X_test) + len(X_val), len(X_train), len(X_test), len(X_val)))

  if config.balance:
    X_train, y_train = balance_data(X_train, y_train)
    X_val, y_val     = balance_data(X_val, y_val)
    X_test, y_test   = balance_data(X_test, y_test)

  # save files.
  save_file(X_train, y_train, 'train', config.out_path)
  save_file(X_test, y_test, 'test', config.out_path)
  save_file(X_val, y_val, 'val', config.out_path)

  return X_train, y_train, X_test, y_test, X_val, y_val

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  
  # io
  parser.add_argument('--document_path', type=str, default="../data/")
  parser.add_argument('--in_path', type=str, default="../data/results/1/")
  parser.add_argument('--out_path', type=str)

  # voting strategy
  vote_config = parser.add_mutually_exclusive_group(required=True)
  vote_config.add_argument('--high_majority', action='store_true')
  vote_config.add_argument('--unanimous', action='store_true')
  vote_config.add_argument('--exact_high_majority', action='store_true')
  vote_config.add_argument('--exact_majority', action='store_true')
  vote_config.add_argument('--majority', action='store_true')

  # utils
  parser.add_argument('--verbose', type=bool, default=True)
  parser.add_argument('--balance', type=bool, default=True)
  parser.add_argument('--clean', type=bool, default=False)
  parser.add_argument('--document_representation', type=bool, default=False)

  # extract data and vote
  config = parser.parse_args()

  main(config)

""" Tests """

def test_balance():
  X = [
    [ "Claim", "Better Evidence", "Worse Evidence"],
    [ "Claim", "Better Evidence", "Worse Evidence"]
  ]
  y = [ 0 , 0 ]
  X_b, y_b = balance_data(X, y)
  assert sum(y_b) == 1
  assert [ "Claim", "Worse Evidence",  "Better Evidence"] in X_b
  assert [ "Claim", "Better Evidence", "Worse Evidence"] in X_b

def test_clean():
  X = [
    [ "Claim", "Better Evidence", "Worse Evidence"],
    [ "Claim", "Better Evidence", "Worse Evidence"]
  ]
  y = [ 0 , 0 ]
  X_b, y_b = clean(X, y)
  assert len(y_b) == 0
  assert len(X_b) == 0
  X = [
    [ 
      "A world in need of better hope is better off not even saving its breath.", 
      "There world lacks hope, and its end is nearing.", 
      "A world in need of better hope is better off not even saving its breath."
    ],
  ]
  y = [ 0 ]
  X_b, y_b = clean(X, y)
  assert len(y_b) == 1
  assert len(X_b) == 1
  X = [
    [ 
      "A world in need of better hope is better off not even saving its breath.", 
      "There world lacks hope, and its end is nearing.", 
      "A world in need of better hope is better off not even saving its breath."
    ],
    [ "Claim", "Better Evidence", "Worse Evidence"]
  ]
  y = [ 0, 1 ]
  X_b, y_b = clean(X, y)
  assert len(y_b) == 1
  assert len(X_b) == 1
