import json

from typing import Dict, List, Tuple

def load_data(path: str, name: str) -> Tuple[List, List]:
  """Loads data files (for X and y) from a given path via a given name.

  Args:
    path (str): the path to the folder.
    name (str): the base name of the files.
  Returns:
    data (Tuple[List, List])
  """
  with open("{}X_{}.json".format(path, name), 'r') as jf:
    X = json.load(jf)
  with open("{}y_{}.json".format(path, name), 'r') as jf:
    y = json.load(jf)
  return X, y

def split(data: List) -> Tuple[List, List, List]:
  """Splits data (evidence, claim, claim) into separate lists. """
  c, s1, s2 = zip(*data)
  return (c, s1, s2)

def get_data(path: str="../../data/results/1-high/") -> Tuple[List, List, List, List, List, List]:
  """Gets a set of data from a folder path. """
  X_test_raw,  y_test  = load_data(path, 'test')
  X_train_raw, y_train = load_data(path, 'train')
  X_val_raw,   y_val   = load_data(path, 'val')

  X_test  = split(X_test_raw)
  X_train = split(X_train_raw)
  X_val   = split(X_val_raw)
  
  return X_train, y_train, X_val, y_val, X_test, y_test

def load_document(path: str, name: str) -> Tuple[List, List]:
  """Loads data files (for X and y) from a given path via a given name.

  Args:
    path (str): the path to the folder.
    name (str): the base name of the files.
  Returns:
    data (Tuple[List, List])
  """
  with open("{}document_{}.json".format(path, name), 'r') as jf:
    return json.load(jf)

def get_document_data(path: str="../../data/results/1-high/") -> Tuple[List[Dict], List[Dict], List[Dict]]:
  data_train = load_document(path, 'train')
  data_val = load_document(path, 'val')
  data_test = load_document(path, 'test')

  return data_train, data_val, data_test
