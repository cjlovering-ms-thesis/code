import torch
from torch.autograd import Variable

class Svm(torch.nn.Module):
  """A simple linear SVM for baseline comparison. """
  
  def __init__(self, sen_vec_dim, out_dim=1):
    """Init params for a simple linear svm.
    Args:
        sen_vec_dim (int): the dimensionality of input feature.
        out_dim (int): The number of classes.
    """
    super(Svm, self).__init__()
    # The embedding includes counts and tfidf.
    self.W = torch.nn.Linear(sen_vec_dim*2, out_dim)

  def forward(self, x):
    c, e = x
    c, e = Variable(torch.Tensor(c)), Variable(torch.Tensor(e))
    if torch.cuda.is_available():
      c = c.cuda()
      e = e.cuda()
    layer_0 = torch.cat((c, e), 1)
    return self.W(layer_0)

