import torch

class RecurrentAdditiveNetworkCell(torch.nn.Module):
  def __init__(self, d_in, d_out, hidden_activation: str='identity'):
    super(RecurrentAdditiveNetworkCell, self).__init__()

    self.d_out = d_out
    self.Wc = torch.nn.Linear(d_in, d_out)

    self.Si = torch.nn.Sigmoid()
    self.Wih = torch.nn.Linear(d_out, d_out)
    self.Wix = torch.nn.Linear(d_out, d_out)
    self.Sf = torch.nn.Sigmoid()
    self.Wfh = torch.nn.Linear(d_out, d_out)
    self.Wfx = torch.nn.Linear(d_out, d_out)

    self.i_gate = lambda x,h: self.Si(self.Wih(h)+self.Wix(x))
    self.f_gate = lambda x,h: self.Sf(self.Wfh(h)+self.Wfx(x))

    if hidden_activation == 'identity':
      self.h_nonlin = lambda x: x

  def forward(self, x):
    if self.c is None:
      batch_size, d_in = x.size()
      self.c = torch.autograd.Variable(torch.FloatTensor(batch_size, d_out))
      self.h = torch.autograd.Variable(torch.FloatTensor(batch_size, d_out))

    i = self.i_gate(x, self.h_prev)
    f = self.f_gate(x, self.h_prev)
    c_hat = self.Wc(x)
    self.c = torch.mul(c_hat, i) + torch.mul(self.c, f)
    self.h = self.h_nonlin(self.c_prev)
    return self.h

class RecurrentAdditiveNetwork(torch.nn.Module):
  def __init__(self, d_in, d_out, hidden_activation: str='identity'):
    super(RecurrentAdditiveNetwork, self).__init__()
    self.d_out = d_out
    self.cell = RecurrentAdditiveNetworkCell(d_in, d_out, hidden_activation)
  
  def forward(self, x):
    seq_len, batch_size, d_in = x.size()
    h = torch.FloatTensor(seq_len, batch_size, self.d_out)
    for step in range(x.size()[0]):
      h[step,:,:] = self.cell(x[step,:,:])
    return h