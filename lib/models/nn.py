import torch
from torch.autograd import Variable

"""
FeedForward networks for simple baselines on sparse features.
The networks concat evidence and claim vectors together to form
a joined feature matrix.
"""

class NN(torch.nn.Module):
  def __init__(self, sen_vec_dim, hidden_1, out_dim=1):
    super(NN, self).__init__()
    self.W1 = torch.nn.Linear(sen_vec_dim*2, hidden_1)
    self.W2 = torch.nn.Linear(hidden_1, out_dim)

  def forward(self, x):
    c, e = x
    c, e = Variable(torch.Tensor(c)), Variable(torch.Tensor(e))
    if torch.cuda.is_available():
      c = c.cuda()
      e = e.cuda()
    layer_0 = torch.cat((c, e), 1)
    layer_1 = self.W1(layer_0).clamp(min=0) # affine + relu
    return self.W2(layer_1)

class NN2(torch.nn.Module):
  def __init__(self, sen_vec_dim, hidden_1, hidden_2, out_dim=1):
    super(NN2, self).__init__()
    self.W1 = torch.nn.Linear(sen_vec_dim*2, hidden_1)
    self.W2 = torch.nn.Linear(hidden_1, hidden_2)
    self.W3 = torch.nn.Linear(hidden_2, out_dim)

  def forward(self, x):
    c, e = x
    c, e = Variable(torch.Tensor(c)), Variable(torch.Tensor(e))
    if torch.cuda.is_available():
      c = c.cuda()
      e = e.cuda()
    layer_0 = torch.cat((c, e), 1)
    layer_1 = self.W1(layer_0).clamp(min=0) # affine + relu
    layer_2 = self.W2(layer_1).clamp(min=0) # affine + relu
    return self.W3(layer_2)
