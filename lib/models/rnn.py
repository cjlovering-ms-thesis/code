import torch
import torchtext.vocab as vocab
import numpy as np
from torch.autograd import Variable

from ..common import loss

class BRNet(torch.nn.Module):
  """BasicRecurrentnet: this model shares a recurrent network for the claim and the evidence. """

  def __init__(self, word_emb_dim, hidden_1, hidden_2, out_dim=1, emb_vocab=vocab.GloVe, rec_net=torch.nn.GRU):
    """Initialize all layers.
    
    Args:
        word_emb_dim (int): size of word embedding.
        hidden_1 (int): size of hidden state of recurrent networks.
        hidden_2 (int): size of hidden state of ending linear layer.
        out_dim (int): the number of out dim (for us always 1).
        emb_vocab (vocab): the type of vocab (Glove vs word2vec).
        rec_net (torch.nn): the type of recurrent network (RNN, Glove, word2vec).
    """
    super(BRNet, self).__init__()
    self.word_emb_dim = word_emb_dim
    self.vocab = emb_vocab(name='6B', dim=self.word_emb_dim)
    self.has_memory = rec_net == torch.nn.LSTM

    self.sen_encoder = rec_net(self.word_emb_dim, hidden_1)

    self.W1 = torch.nn.Linear(hidden_1*2, hidden_2)
    self.W2 = torch.nn.Linear(hidden_2, out_dim)

  def forward(self, x):
    """Forward pass on input sentences, predicting a score for each pair of claim & evidence.
    
    Args:
        x - List(List(string)): Each string is a sentence.
    """
    # split into claim and evidence.
    c, e = x
    
    # embed all words using chosen embedding (GloVe).
    c = self.get_batch(self.prepare_batch(c))
    e = self.get_batch(self.prepare_batch(e))

    # process sentences through a sentence encoder (GRU).
    c_h, _ = self.sen_encoder(c)
    e_h, _ = self.sen_encoder(e)

    # select last hidden state as context vector.
    c_h_n = c_h[:,-1,:] 
    e_h_n = e_h[:,-1,:]

    # join the hidden layers and run them through a rectified affine layer.
    context = torch.cat([c_h_n, e_h_n], 1)
    layer = self.W1(context).clamp(min=0)
    
    # predict scores
    return self.W2(layer)

  def prepare_batch(self, x):
    """ Args: x - [string, ... ] """
    split = [ s.split() for s in x ]
    filtered = [
      [ w for w in s if w in self.vocab.stoi ] 
      for s in x
    ]
    shortened = [
      s if len(s) < 100 else s[:100] for s in x
    ]
    return shortened

  def get_batch(self, batch):
    """Embeds a batch of sentences. """
    batch_size = len(batch)
    max_len = max([len(s) for s in batch])
    embed = np.zeros((batch_size, max_len, self.word_emb_dim))

    for i in range(len(batch)):
      for j in range(len(batch[i])):
        embed[i, j, :] = self.get_word(batch[i][j])

    if torch.cuda.is_available():
      return Variable(torch.FloatTensor(embed)).cuda()
    return Variable(torch.FloatTensor(embed))

  def get_word(self, word):
    """Returns the embedding for a word. """
    if word not in self.vocab.stoi:
      return self.vocab.vectors[0]
    return self.vocab.vectors[self.vocab.stoi[word]]
