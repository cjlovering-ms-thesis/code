# src

Source for DL models.

## .src

`results.ipynb` notebook for exploring models.

## common

Functions shared between models and misc. Auxillary training utilities, loss functions, etc.

## input

Different input representations. Largely unused.

## models

Models for capturing pairwise ranking.

## modules

Modules (parts of models) like highway layers, attention... that build up our models.
