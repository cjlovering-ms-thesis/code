from .common import *

from .modules import Attention, Highway, SimilarityMatrix
from .models import BiDafR
from .common import load, predict

from .utils import normalize, get_partition