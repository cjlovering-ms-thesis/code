import json
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

class Sparse():
    def __init__(self, _vectorizers=[CountVectorizer, TfidfVectorizer], path="../../data/clean/sentences.txt"):
        self.vectorizers = [V(ngram_range=(1,3), max_features=5000) for V in _vectorizers]
        
        with open(path, 'r') as f:
            text = f.readlines()
        for V in self.vectorizers:
            V.fit(text)

    def transform(self, data):
        out = []
        for V in self.vectorizers:
            out.append(V.transform(data))
        return np.hstack([d.toarray() for d in out])
