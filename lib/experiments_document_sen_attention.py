"""This script is used to evaluate our models on increasing amounts of data. """
import argparse
import numpy as np
import json
import csv
import json
import uuid
import torch

import pandas as pd
import matplotlib.pyplot as plt

##

import logging

##

from modules.highway import Highway
from modules.attention import Attention
from modules.similarity_matrix import SimilarityMatrix
from modules.attention_linear import AttentionLinear

import torch
import torchtext.vocab as vocab
import numpy as np

from torch.autograd import Variable

class HanR(torch.nn.Module):
  """Han-r: this model shares a recurrent network for the claim and the evidence. """
  def __init__(self, word_emb_dim: int=200, hidden_size: int=100, out_dim: int=1, emb_vocab=vocab.GloVe,
      encoder: torch.nn.Module=torch.nn.LSTM, num_enc_layers: int=1, bidirectional: bool=False, dropout: float=0.,
      highway_layers_wrd: int=0, highway_layers_seq: int=0, attend_claim: bool=False,
      attend_evidence: bool=False, num_encode_sen: int=1, attend_sen: bool=False, cuda: bool=False) -> None:
    """Initialize all layers.

    Args:
        word_emb_dim (int): size of word embedding.
        hidden_size (int): size of hidden state of recurrent networks.
        out_dim (int): the number of out dim (for us always 1).
        emb_vocab (vocab): the type of vocab (Glove vs word2vec).
        rec_net (torch.nn): the type of recurrent network (RNN, Glove, word2vec).
        bidirectional (bool): if true recurrent networks are bi-directional.
        attention (bool): if true add attention
    """
    super(HanR, self).__init__()
    self.word_emb_dim = word_emb_dim
    self.vocab = emb_vocab(name='6B', dim=self.word_emb_dim)
    self._volatile = False
    self.cuda = cuda

    # char level operations.
    ## if highway init highway layers for words
    self.highway_wrd = highway_layers_wrd > 0
    if self.highway_wrd:
      self.highway_wrd_network = Highway(input_size=word_emb_dim, num_layers=highway_layers_wrd)

    ## determine how big final layer is.
    context_size = hidden_size # * 3
    similarity_size = hidden_size

    ## if bidirectional, reduce hidden size by x2 to keep # of params.
    hidden_size = hidden_size // 2 if bidirectional else hidden_size

    ## sentence context encoders
    self.c_seq_encoder = encoder(input_size=self.word_emb_dim, hidden_size=hidden_size, \
        num_layers=num_enc_layers, bidirectional=bidirectional, dropout=dropout)
    self.e_seq_encoder = encoder(input_size=self.word_emb_dim, hidden_size=hidden_size, \
        num_layers=num_enc_layers, bidirectional=bidirectional, dropout=dropout)

    ## attention
    self.attend_claim = attend_claim
    self.attend_evidence = attend_evidence
    if attend_claim or attend_evidence:
      self.similarity_matrix = SimilarityMatrix(hidden_size=similarity_size)
      self.attend = Attention()

    ## if highway init highway layers for seq representation
    self.highway_seq = highway_layers_seq > 0
    if self.highway_seq:
      self.highway_seq_network = Highway(input_size=context_size, num_layers=highway_layers_seq)
      self.highway_claim_network = Highway(input_size=context_size, num_layers=highway_layers_seq)

    # sentence level operations.
    ## sentence level encoder.
    self.encode_sen = num_encode_sen > 0
    if self.encode_sen:
      self.sen_encoder = encoder(input_size=context_size, hidden_size=hidden_size, \
        num_layers=num_enc_layers, bidirectional=bidirectional, dropout=dropout)

    ## sentence level attention.
    self.attend_sen = attend_sen
    if self.attend_sen:
      self.sen_attend = AttentionLinear(context_size)

    self.final_affine = torch.nn.Linear(context_size*2, out_dim, bias=False)

  def forward(self, x):
    """Forward pass on input sentences, predicting a score for each pair of claim & evidence.

    Args:
        x - List(List(string)): Each string is a sentence.
    """
    # split into claim and evidence.
    c, e = x
    c = [c]

    # embed all words using chosen embedding (default: GloVe).
    c = self.get_batch(self.prepare_batch(c))
    e = self.get_batch(self.prepare_batch(e))

    # setup highway for encoding words.
    if self.highway_wrd:
      c = self.highway_wrd_network(c)
      e = self.highway_wrd_network(e)

    # process sentences through a sentence encoder (default: GRU).
    c_u, _ = self.c_seq_encoder(c)
    e_u, _ = self.e_seq_encoder(e)
    
    # tile the claim for each sentence in the evidence.
    if self.attend_claim or self.attend_evidence:  
      tiled_c_u = c_u.repeat(e_u.size()[0], 1, 1)
      mtx = self.similarity_matrix(tiled_c_u, e_u)

#    claim_representation = []
    
#     if self.attend_claim:
#       c_a = self.attend(tiled_c_u, mtx)
#       claim_representation.append(c_a)
#     else:
#       c_u_l = tiled_c_u[:,-1,:]
#       claim_representation.append(c_u_l)

    if self.attend_evidence:
      e_a = self.attend(e_u, mtx)
      features = e_a
    else:
      e_u_l = e_u[:,-1,:]
      features = e_u_l

    # join features, and then: 
    # features = torch.cat([*features, torch.mul(*features)], 1).unsqueeze(1)
    
    # (64,24) => (64,1,24)
    sentences = features.unsqueeze(1)

    claim_representation = c_u[:,-1,:].squeeze(0)
    
    # sentence level operations
    if self.encode_sen:
      context_aware_sentences, _ = self.sen_encoder(sentences)
      context_aware_sentences = context_aware_sentences.squeeze(1)
      sentences = context_aware_sentences
    else:
      sentences = features
    
    if self.attend_sen:
      attended_sentences = self.sen_attend(sentences, claim_representation)
      sentences = attended_sentences

    if self.highway_seq:
      sentences = encoded_sentences = self.highway_seq_network(sentences)
      claim_representation = encoded_claim_representation = self.highway_claim_network(claim_representation)

    # features = torch.cat([encoded_sentences, torch.mul(encoded_sentences, tiled_c_u), tiled_c_u], 1).unsqueeze(1)
    final_features = torch.cat([sentences, torch.mul(sentences, claim_representation)], 1)
    
    return self.final_affine(final_features)

  def prepare_batch(self, x):
    """ Args: x - [string, ... ] """
    split = [ s.split() for s in x ]
    filtered = [
        [ w for w in s if w in self.vocab.stoi ]
        for s in x
    ]
    MAX_SIZE = 128
    shortened = [
        s[:MAX_SIZE] for s in filtered
    ]
    return shortened

  def get_batch(self, batch):
    """Embeds a batch of sentences.

    Returns:
      (Variable): Embedded word matrix.
      (IntTensor): Lengths of each sentence.
    """

    batch_size = len(batch)
    max_len = max([len(s) for s in batch])
    embed = torch.zeros(batch_size, max_len, self.word_emb_dim)

    for i in range(batch_size):
      for j in range(len(batch[i])):
        embed[i, j, :] = self.get_word(batch[i][j])

    if torch.cuda.is_available() and self.cuda:
        return Variable(embed, volatile=self._volatile).cuda()
    return Variable(embed, volatile=self._volatile)

  def get_word(self, word):
    """Returns the embedding for a word. """
    if word not in self.vocab.stoi:
        return self.vocab.vectors[0]
    return self.vocab.vectors[self.vocab.stoi[word]]

  def volatile(self, volatile):
    """Sets state to generate volatile vectors. """
    self._volatile = volatile

from torch.autograd import Variable
from common.document_training import train
from common.grid import generate, harness, explore_grid, grid_search
from utils.data_io import get_document_data

MAX_TRN_COUNT = 35000
MAX_VAL_COUNT = 10000

EPOCHS_SMALL_DS = 15
RECORD_SMALL_DS = 1

from collections import defaultdict
results = defaultdict(list)

def sample_document(data, k=25):
  total = 0
  out = []
  for doc in data:
    out.append(doc)
    total += len(doc['y'])
    if total > k:
      break
  return out

def shuffle(data):
  def get(sentences, mask):
    return [ sentences[i] for i in mask ]
  mask = np.random.choice(len(data), len(data))
  return get(data, mask)

def main(config):
  
  logging.basicConfig(filename='{}.csv'.format(config.log_file),level=logging.INFO)

  train_data, val_data, test_data = get_document_data(config.in_path)
  train_data = sample_document(train_data, MAX_TRN_COUNT)
  val_data= sample_document(val_data, MAX_VAL_COUNT)

  for _ in range(3):

      # shuffle data
      train_data = shuffle(train_data)

      # super sets
      X_0 = sample_document(train_data,   500)
      X_1 = sample_document(train_data,  1000)
      X_2 = sample_document(train_data,  2200)
      X_3 = sample_document(train_data,  4375)
      X_4 = sample_document(train_data,  8750)
      X_5 = sample_document(train_data, 17500)
      X_6 = sample_document(train_data, 35000)

      data = [
        X_0, X_1, X_2, X_3, X_4, X_5, X_6
      ]

      for i,train_data_split in enumerate(data):
          hyperparams = {
            'config':{
                'bidirectional': True,
                'hidden_size': 200,
                'num_enc_layers': 1,
                'encoder': torch.nn.LSTM,
                'attend_claim': False,
                'attend_evidence': False,
                'attend_sen': True
            },
                'optim' : torch.optim.Adam,
                'model_class' : HanR,
                'epochs' : EPOCHS_SMALL_DS,
                'record_val' : RECORD_SMALL_DS
          }
          _, history = train(train_data_split, val_data, **hyperparams)
          document_len = lambda doc: sum([len(d['y']) for d in doc])
          logging.info("{}, {}, {}".format('train', document_len(train_data_split), history['train']['acc']))
          logging.info("{}, {}, {}".format('test', document_len(val_data),  history['val']['acc']))
  exit()

if __name__ == "__main__":
  parser = argparse.ArgumentParser()

  # io
  parser.add_argument('--in_path', type=str, default="../data/results/argument/")
  parser.add_argument('--log_file', type=str, default="experiment_results")

  # extract data and vote
  config = parser.parse_args()

  main(config)