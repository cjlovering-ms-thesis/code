"""This script is used to evaluate our models on increasing amounts of data. """
import argparse
import numpy as np
import json
import csv
import json
import uuid
import torch

import pandas as pd
import matplotlib.pyplot as plt
from common.document_training_two import train

import logging

from hanr import HanR

from torch.autograd import Variable
from utils.data_io import get_document_data

from typing import Dict

def get_hyperparams(config_mode: str) -> Dict:
  if config_mode == "basic":
    hyperparams = {
      'config':{
          'bidirectional': True,
          'hidden_size': 80,
          'num_enc_layers': 1,
          'encoder': torch.nn.LSTM,
          'attend_claim': False,
          'attend_evidence': False,
      },
          'optim' : torch.optim.Adam,
          'model_class' : HanR,
          'epochs' : EPOCHS_SMALL_DS,
          'record_val' : RECORD_SMALL_DS
    }
  if config_mode == "wrd_attention":
    hyperparams = {
      'config':{
          'bidirectional': True,
          'hidden_size': 80,
          'num_enc_layers': 1,
          'encoder': torch.nn.LSTM,
          'attend_claim': True,
          'attend_evidence': True,
          'attend_sen': False
      },
          'optim' : torch.optim.Adam,
          'model_class' : HanR,
          'epochs' : EPOCHS_SMALL_DS,
          'record_val' : RECORD_SMALL_DS
    }
  if config_mode == "sen_attention":
    hyperparams = {
      'config':{
          'bidirectional': True,
          'hidden_size': 80,
          'num_enc_layers': 1,
          'encoder': torch.nn.LSTM,
          'attend_claim': False,
          'attend_evidence': False,
          'attend_sen': True
      },
          'optim' : torch.optim.Adam,
          'model_class' : HanR,
          'epochs' : EPOCHS_SMALL_DS,
          'record_val' : RECORD_SMALL_DS
    }
  if config_mode == "full_attention":
    hyperparams = {
      'config':{
          'bidirectional': True,
          'hidden_size': 80,
          'num_enc_layers': 1,
          'encoder': torch.nn.LSTM,
          'attend_claim': True,
          'attend_evidence': True,
          'attend_sen': True
      },
          'optim' : torch.optim.Adam,
          'model_class' : HanR,
          'epochs' : EPOCHS_SMALL_DS,
          'record_val' : RECORD_SMALL_DS
    }
  return hyperparams

def sample(X,k):
    a,b,c = X
    return (a[:k], b[:k], c[:k])

MAX_TRN_COUNT = 35000
MAX_VAL_COUNT = 10000

EPOCHS_SMALL_DS = 15
RECORD_SMALL_DS = 1

from collections import defaultdict
results = defaultdict(list)

def sample(X, y, k=25,z=0):
    a,b,c = X
    return (a[z:z+k], b[z:z+k], c[z:z+k]), y[z:z+k]

def shuffle(X, y):
  def get(sentences, mask):
    return [ sentences[i] for i in mask ]

  C, E1, E2 = X
  mask = np.random.choice(len(y), len(y))
  BC, BE1, BE2 =  get(C, mask), get(E1, mask), get(E2, mask)
  By = get(y,mask)
  return (BC, BE1, BE2), By

def main(config):
  train_filename='train_{}.csv'.format(config.log_file)
  test_filename='test_{}.csv'.format(config.log_file)
  train_data, val_data, test_data = get_document_data(config.in_path)

  hyperparams = get_hyperparams(config.config_mode)

  for _ in range(config.restarts):
      _, history = train(train_data, val_data, **hyperparams)
      csv_log(train_filename, "{}".format(history['train']['acc']))
      csv_log(test_filename, "{}".format(history['val']['acc']))
  exit()

def csv_log(file, message):
  with open(file, 'a') as f:
    f.write("{}\n".format(message))

if __name__ == "__main__":
  parser = argparse.ArgumentParser()

  # io
  parser.add_argument('--in_path', type=str, default="../data/results/document/")
  parser.add_argument('--log_file', type=str, default="experiment_results")

  # experiment
  parser.add_argument('--config_mode', type=str, default="basic")
  parser.add_argument('--restarts', type=int, default=1)

  # extract data and vote
  config = parser.parse_args()

  main(config)