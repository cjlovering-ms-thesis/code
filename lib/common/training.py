import torch
import gc

import numpy as np

from collections import defaultdict
from torch.autograd import Variable
from typing import Any, Dict, List, Tuple

from torch.optim.lr_scheduler import ReduceLROnPlateau

from .loss import  pairwise_acc, pairwise_loss, pairwise_loss_ranking

def train(X, y, X_val, y_val, lr=1e-3, reg=0, optim=torch.optim.SGD, model_class=None, config=None, epochs=50, record_val=5, title=None, batch_size=32):
  r"""Training function that has the primary responsibility of batching data & organizing epochs.
  
  Args:
    X (list(list(string)) | X(list(list(ndarray))): The training data.
    y (ndarray): The training labels.
    X_val (list(list(string)) | X(list(list(ndarray))): The validation data.
    y_val (ndarray): The validation labels.
    lr (float): The learning rate. TODO: allow for lr schedules.
    reg (float): Weight decay.
    optim (torch.optim): Optimizer/learner.
    model_class (torch.nn.Module): The NN class to train.
    config (object): Parameter pairs for the given model.
    epochs (int): Number of epochs to train. 
    record_val (int): Store history every record_val epoch. User `-1` to turn this off.
  Returns:
    model (torch.Optim): Returns the trained model.
    history (object): Contains the history of the training.
  """
  
  # convert from list to numpy
  y = np.array(y)
  y_val = np.array(y_val)

  # set paramaters.
  num_val = len(X_val[0])
  num_train = len(X[0])
  batch_size = min(num_train, batch_size)

  # construct our model by instantiating the class defined above.
  model = model_class(**config)
  
  # put model on gpu if cuda is available.
  if torch.cuda.is_available():
    model = model.cuda()
  
  # construct our loss function w/ optimizer
  if lr >= 0:
    # lr is manually configured.
    optimizer = optim(model.parameters(), lr=lr, weight_decay=reg)
  else:
    # use default lr.
    optimizer = optim(model.parameters(), weight_decay=reg)

  # reduce lr when loss flattens.
  scheduler = ReduceLROnPlateau(optimizer, patience=2)
  
  history = { 
    'train': { 'loss': [], 'acc': [] }, 
    'val':   { 'loss': [], 'acc': [] }
  }

  for t in range(epochs):
    model.train()
    model.volatile(False)
    for batch in range(num_train // batch_size + (num_train % batch_size > 0)):         
    
      # fetch batch
      X_batch, y_batch = get_batch(X, y, num_train, batch_size)
      
      # construct inputs of Claims & Evidence
      C_batch, E1_batch, E2_batch = X_batch
      
      X1 = C_batch, E1_batch
      X2 = C_batch, E2_batch
      
      # forward pass outputting scores
      y_1 = model(X1)
      y_2 = model(X2)
      
      # compute loss
      loss = pairwise_loss_ranking(y_1, y_2, y_batch)

      # zero gradients, perform a backward pass, and update the weights.
      optimizer.zero_grad()
      loss.backward()
      optimizer.step()

      # clean memory per batch. Cannot del y_1, y_2 because its used for tracking.
      del loss
      gc.collect()

    if t != epochs - 1:
      continue

    # sample validation data to keep size small.
    y_1_val_l, y_2_val_l, y_val_l = [], [], []
    model.eval()
    model.volatile(True) # custom method for setting variables to volatile for eval.
    for batch in range(num_val // batch_size + (num_val % batch_size > 0)):
      X_val_sample_batch, y_val_sample_batch = get_batch(X_val, y_val, num_val, batch_size, batch=batch)
      C_val_sample_batch, E1_val_sample_batch, E2_val_sample_batch = X_val_sample_batch
      
      X1_val_sample = C_val_sample_batch, E1_val_sample_batch
      X2_val_sample = C_val_sample_batch, E2_val_sample_batch
  
      # step scheduler.
      y_1_val = model(X1_val_sample)
      y_2_val = model(X2_val_sample)

      # stash results
      y_1_val_l.append(y_1_val)
      y_2_val_l.append(y_2_val) 
      y_val_l.append(y_val_sample_batch)

    # if validation data was greater than 1 batch concat results together, otherwise don't.
    if len(y_1_val_l) > 1:
      val_loss = pairwise_loss(torch.cat(y_1_val_l, 0), torch.cat(y_2_val_l, 0), torch.cat(y_val_l, 0)).data[0]
    else: 
      val_loss = pairwise_loss(y_1_val_l[0], y_2_val_l[0], y_val_l[0]).data[0]
    scheduler.step(val_loss)

    # record values
    if record_val > 0 and t % record_val == 0: 
      history = record(model, history, y_1_val, y_2_val, y_val_sample_batch, y_1, y_2, y_batch)

    # stopping condition based on scheduler
    if optimizer.state_dict()['param_groups'][-1]['lr'] < 1e-7:
      print('Learning is finished after {} epochs.'.format(t))
      break

    # clean up to reduce memory usage: i.e. leave no hanging variables.
    del y_1, y_2, y_1_val, y_2_val, y_1_val_l[:], y_2_val_l[:], y_val_l[:], val_loss
    gc.collect()

  return model, history

def get_batch(X: List[List[str]], y: List[int], num_train: int, batch_size: int, 
    batch: int=None) -> Tuple[Any, Any]:
  r"""Fetches a batch out of the data. For now no dataloader is necessary.

  Args:
    X (zipped(list(string), list(string), list(string))): Training data.
    y (list(int)): The training labels.
    num_train (int): The number of training points.
    batch_size (int): The size of the batch.
    batch (int): Index of the batch. If true, does not randomize input.
  Returns:
    X_batch (zipped(list(string), list(string), list(string))): Batched training data.
    y_batch (Variable): Batched labels.
  """
  def get(sentences: List, mask) -> List:
    return [ sentences[i] for i in mask ]
  C, E1, E2 = X
  mask = np.random.choice(num_train, batch_size)

  if batch is None:
    BC, BE1, BE2 =  get(C, mask), get(E1, mask), get(E2, mask)
    By = y[mask]
  else:
    start = batch_size*batch
    BC, BE1, BE2 =  C[start:start+batch_size], E1[start:start+batch_size], E2[start:start+batch_size] 
    By = y[start:start+batch_size]

  X_batch = BC, BE1, BE2
  y_batch = Variable(torch.from_numpy(np.array(By)), requires_grad=False).float()
  if torch.cuda.is_available():
    y_batch = y_batch.cuda()
  return X_batch, y_batch

def record(model: torch.nn.Module, history: Dict, y_1_val: Variable, y_2_val: Variable,
    y_val: Variable, y_1_train: Variable, y_2_train: Variable, y_train: Variable) -> Dict:
  r""" Operates on validation data & stores training results. """

  val_loss = pairwise_loss(y_1_val, y_2_val, y_val)
  val_acc  = pairwise_acc(y_1_val, y_2_val, y_val)

  history['val']['loss'].append(val_loss.data[0])
  history['val']['acc'].append(val_acc.data[0])

  big_train_loss = pairwise_loss(y_1_train, y_2_train, y_train)
  big_train_acc  = pairwise_acc(y_1_train, y_2_train, y_train)

  history['train']['loss'].append(big_train_loss.data[0])
  history['train']['acc'].append(big_train_acc.data[0])

  return history
