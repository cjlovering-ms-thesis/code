import torch
import numpy as np

from typing import List, Dict, Any

from .training import train
from .plots import plot_histories, plot_histories_vertical
from .save import save_history, save_model, to_title

def harness_single(X_train, y_train, X_val, y_val, hyperparam, group_title,
      _save_model=True, _save_history=True, save_plots=False, train_f=train) -> None:
  r"""Trains the model given the hyperparameters.

  note::
    Used for splitting val/abalations across a cluster.
  """
  model, history = train_f(
      X_train, np.array(y_train), X_val, y_val,
      **hyperparam)
  record_val = hyperparam['record_val']
  model_name = "{}-{}".format(group_title.replace(" ", ""), to_title(hyperparam))
  if _save_model:
    save_model(model_name, model)
  results = [(history, hyperparam)]
  if _save_history:
    save_history(results, model_name, record_val=record_val)
  plot_histories_vertical(
    results, group_title, record_val=record_val, save_plots=save_plots, save_name=model_name)

def grid_search(X_train, y_train, X_val, y_val, setup, train_f=train, document=False, runs: int=5) -> List:
  r"""Trains all the different models and parameters in the setup.

  Args:
    runs (int): the number of times to repeat each train. we do this to reduce variance.

  Returns: A dict of lists of models.
  """
  best_acc = 0
  best_params = None

  result_set = []

  assert runs > 0

  for group in setup:

    details = group['details']
    group_title = group['title']
    results = []

    for hyperparam in details:
      hyperparam_results = []

      for r in range(runs):
        if document:
          _, history = train_f(X_train, X_val, **hyperparam)
        else:
          _, history = train_f(
              X_train, np.array(y_train), X_val, y_val,
              **hyperparam)

        hyperparam_results.append(history)

        acc = history['val']['acc'][-1]

        if acc > best_acc:
          best_acc    = acc
          best_params = hyperparam

      # save hyperparam results
      results.append((hyperparam, hyperparam_results))

    # hyperparam is left hanging from previous loop.
    record_val = hyperparam['record_val']

    # save results
    result_set.append(results)

  return best_acc, best_params, result_set

def harness(X_train, y_train, X_val, y_val, setup, model_path=None, train_f=train, plot=True) -> List:
  r"""Trains all the different models and parameters in the setup.

  Returns: A dict of lists of models.
  """
  result_set = []
  for group in setup:
    details = group['details']
    group_title = group['title']
    results = []

    for hyperparam in details:
      model, history = train_f(
          X_train, np.array(y_train), X_val, y_val,
          **hyperparam)

      model_name = "{}-{}".format(group_title.replace(" ", ""), to_title(hyperparam))
      results.append((history, hyperparam))
      save_model(model_name, model)

      # leaving hanging for outside loop.
      record_val = hyperparam['record_val']

    # save results and plot metrics
    save_history(results, group_title.replace(" ", ""), record_val=record_val)
    if plot: plot_histories_vertical(results, group_title, record_val=record_val)
    result_set.append(results)
  return result_set

def generate(key: str, values: List, base: Dict, big_title: str, value_names: list=None):
  r"""Generates a group of trial configurations to train.

  Args:
    key (str): The param to change.
    values (list): The options to use.
    base (Dict): The original set of parameters.
    big_title (string): Descriptor of the output group.
    value_names (list): Optional. Description of each value.
  Returns:
    A group of config to be trained.
  """
  out = []
  for i,v in enumerate(values):
    name = v
    if value_names is not None:
      name = value_names[i]
    out.append({
      **base,
      key: v,
      'title': "{}{}".format( base['title'], name ),
    })
  return {
    'title': big_title,
    'details': out
  }

def explore_grid(key: str, values: List, base: Dict, big_title: str):
  r"""Generates a group of trial configurations to train.

  Args:
    key (string): The param to change.
    values (list): The options to use. Each option is a range of params to try.
    base (object): The original set of parameters.
    big_title (string): Descriptor of the output group.
  Returns:
    A group of config to be trained.
  """
  out = []
  grid = generate_grid(values)
  for v in grid:
    out.append({
      **base,
      key: v,
    })
  return {
    'title': big_title,
    'details': out
  }

def generate_grid(values: Dict) -> List[Dict[str, Any]]:
  r"""Generates all combinations.

  Args:
    values (Dict): Dictionary of param name to list of options.
  Returns:
    out (List): List of all combinations with param names.
  """
  out = []
  for k,r in values.items():
    if not out:
      for v in r:
        out.append( { k: v } )
    else:
      new_out = []
      for prev in out:
        for v in r:
          new_out.append( { **prev, k: v } )
      out = new_out
  return out

""" tests """
def test_generate_grid():
  out =  generate_grid( { 'a': [1], 'b': [3] })
  assert out == [ { 'a': 1, 'b': 3 } ]

  out =  generate_grid( { 'a': [1, 2], 'b': [3, 4] })
  assert out == [ { 'a': 1, 'b': 3 }, { 'a': 1, 'b': 4 }, { 'a': 2, 'b': 3 }, { 'a': 2, 'b': 4 } ]

  out =  generate_grid( { 'a': [1], 'b': [2, 3, 4] })
  assert out == [ { 'a': 1, 'b': 2 }, { 'a': 1, 'b': 3 }, { 'a': 1, 'b': 4 } ]