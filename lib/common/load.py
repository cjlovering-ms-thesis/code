import torch

from typing import Any, Dict

def load(model_path: str=None, model_class: torch.nn.Module=None, config: Dict[str, Any]={}) -> torch.nn.Module:
  """Loads a model.

  Args:
    model_path (str): the path to a trained model (or model state) saved to file.
    model_class (torch.nn.Module): the type of the model; if None, model was saved entirely to file.
    config (Dict[str, Any]): model hyperparam definition.
  Returns:
    model: trained instance of the model.
  """
  if model_class is not None:
    model = model_class(**config)
    model.load_state_dict(torch.load(model_path))
  else:
    model = torch.load(model_path)

  if torch.cuda.is_available():
    model.cuda()

  return model
