import torch
import json
import datetime

from typing import List, Tuple, Dict, Any
from copy import deepcopy

def save_history(results: List[Tuple[Dict[str, List[float]], Dict[str, Any]]],
    group_title: str = "", record_val: int = 1) -> None:
  """Saves results and hyperparam for post-processing.

  Args:
    results (List[Tuple[Dict[str, List[float]], Dict[str, Any]]]): see example.
  Example::
    results = [
      (
        {
          'train': {
            'auc': [ 0.10, 0.90 ],
            'acc': [ 0.98, 0.99 ],
            'loss': [ 3.0, 0.70 ],
          },
          'val': {
            'auc': [ 0.10, 0.90 ],
            'acc': [ 0.98, 0.99 ],
            'loss': [ 3.0, 0.70 ],
          }
        },
        {
          'reg': 0.5,
          'optim': torch.optim.Adam,
          'config': {
            'attention': True,
            'hidden_size': 80
          }
        }
      ), ...
    ]
    save_history(results)
  """
  hyperparams = results[0][1]
  params = to_param_list(hyperparams)
  cleaned = []
  for history, hyperparam in results:
    config = to_value_list(hyperparam)
    cleaned.append({
      'history': history,
      'config': config
    })
  formatted = {
    'record_val': record_val,
    'params': params,
    'results': cleaned
  }
  now = datetime.datetime.now().strftime("%d-%H-%M-%S")
  with open('../checkpoints/history/{}-{}.json'.format(group_title, now), 'w') as jf:
    json.dump(formatted, jf, indent=1)

def save_model(name: str, model: torch.nn.Module) -> None:
  r"""Saves model state with name. """
  torch.save(model.state_dict(), '../checkpoints/models/{}-state.pt'.format(name))

def to_title(hyperparams: Dict) -> str:
  r"""Get shortened title for set of hyperparams. """
  params = __get_param_list(hyperparams)
  values = __get_param_values(hyperparams)
  pairs = zip(params, values)
  title = ["{}:{}".format(param,value) for param, value in pairs if param is not None]
  return ",".join(title)

def to_param_list(hyperparams: Dict) -> str:
  r"""Get shortened title for set of hyperparams. """
  params = __get_param_list(hyperparams)
  values = __get_param_values(hyperparams)
  pairs = zip(params, values)
  return [param for param, value in pairs if param is not None]

def to_value_list(hyperparams: Dict) -> List[Any]:
  r"""Get shortened title for set of hyperparams. """
  params = __get_param_list(hyperparams)
  values = __get_param_values(hyperparams)
  pairs = zip(params, values)
  return [value for param, value in pairs if param is not None]

def __get_param_list(hyperparams: Dict) -> List[str]:
  r"""Get the abbreviated names of the params.

  Returns:
    (List): List of str and None where params should be skipped.
  """
  flattened = flatten_hyperparam(hyperparams)
  return [get_abbrev(param) for param in list(flattened.keys())]

def __get_param_values(hyperparams: Dict) -> List[Any]:
  r"""Get the formatted values of the params. """
  flattened = flatten_hyperparam(hyperparams)
  return [get_param_value(value) for value in list(flattened.values())]

def flatten_hyperparam(hyperparams: Dict) -> Dict:
  r"""Flattens config into hyperparams. """
  config = hyperparams['config']
  _hyperparams = { k:v for k,v in deepcopy(hyperparams).items() if k != 'config' }
  return {**_hyperparams, **config}

def get_param_value(val: Any) -> str:
  r"""Returns param value in str form for saving.

  Args:
    val (Any): optim, nn, bool, float, or int.
  Returns:
    (str): the string version.
  """
  val_type = type(val)

  if val_type is bool:
    if val: return 'T'
    return 'F'

  if val_type is float:
    return val
  if val_type is int:
    return val

  if val is torch.optim.Adamax:
    return 'Adamax'
  if val is torch.optim.Adam:
    return 'Adam'
  if val is torch.optim.Adagrad:
    return 'Adagrad'
  if val is torch.optim.Adadelta:
    return 'Adadelta'
  if val is torch.optim.SGD:
    return 'Momentum'

  if val is torch.nn.GRU:
    return 'GRU'
  if val is torch.nn.LSTM:
    return 'LSTM'

  return str(val)

def get_abbrev(param: str) -> Any:
  r"""Abbreviates the param to be saved.

  Args:
    param (str): A hyperparam or abalation for the network.
  Returns:
    (str): Abbreviated version of param. None if not to be shown.
  """
  lookup = {
    'hidden_size': 'h_s',
    'word_emb_dim': 'w_s',
    'dropout': 'drop',
    'num_enc_layers': 'lrs',
    'bidirectional': 'bi',
    'out_version': 'out',
    'attend_claim': 'att_c',
    'attend_evidence': 'att_e',
    'highway_layers_wrd': 'h_way_w',
    'highway_layers_seq': 'h_way_s',
    'encoder': 'enc',
    'lr' : None,
    'reg' : 'reg',
    'optim' : 'optim',
    'model_class' : None,
    'epochs' : None,
    'record_val' : None,
    'title': None,
    'out_dim': None,
    'batch_size': None
  }
  return lookup[param]

""" tests """

def test_get_param_value():
  assert get_param_value(torch.optim.Adadelta) == 'Adadelta'
  assert get_param_value(torch.optim.Adam) == 'Adam'
  assert get_param_value(torch.optim.Adamax) == 'Adamax'
  assert get_param_value(torch.optim.Adagrad) == 'Adagrad'
  assert get_param_value(torch.optim.SGD) == 'Momentum'

def test_to_params_list_example():
  params = to_param_list({
    'config': {
      'attend_evidence': False,
      'attend_claim': False,
      'bidirectional': False,
      'dropout': 0.1,
      'hidden_size': 8,
      'highway_layers_seq': 0,
      'highway_layers_wrd': 0,
      'num_enc_layers': 1
    },
    'epochs': 1,
    'lr': -1,
    'model_class': "asdf",
    'optim': torch.optim.Adamax,
    'record_val': 1,
    'reg': 0.01
  })
  assert params == ['optim', 'reg', 'att_e', 'att_c', 'bi', 'drop', 'h_s', 'h_way_s', 'h_way_w', 'lrs' ]

def test_to_values_list_example():
  params = to_value_list({
    'config': {
      'attend_evidence': False,
      'attend_claim': False,
      'bidirectional': False,
      'dropout': 0.1,
      'hidden_size': 8,
      'num_enc_layers': 1
    },
    'epochs': 1,
    'lr': -1,
    'model_class': "asdf",
    'optim': torch.optim.Adamax,
    'record_val': 1,
    'reg': 0.01
  })
  assert params == ['Adamax', 0.01, 'F', 'F', 'F', 0.1, 8, 1]

def test_to_title_example():
  title = to_title({
    'config': {
      'bidirectional': False,
      'dropout': 0.1,
      'hidden_size': 8,
      'num_enc_layers': 1
    },
    'epochs': 1,
    'lr': -1,
    'model_class': "asdf",
    'optim': torch.optim.Adamax,
    'record_val': 1,
    'reg': 0.01
  })
  assert title == "optim:Adamax,reg:0.01,bi:F,drop:0.1,h_s:8,lrs:1"

def test_to_title():
  title = to_title({
    'model_class': 'n/a', 'reg' : 0.01,
    'config':  {'hidden_size': 100, 'word_emb_dim': 200, 'dropout': 0.0, 'num_enc_layers': 1, 'bidirectional': True}
  })
  assert title == "reg:0.01,h_s:100,w_s:200,drop:0.0,lrs:1,bi:T"
