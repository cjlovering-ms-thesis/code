import torch

from typing import Any, Dict, List, Tuple

def predict(X: Tuple[List[str], List[str]], model: torch.nn.Module=None) -> Dict[int, float]:
  """Evalutes saved model on the given X.
  """
  if torch.cuda.is_available():
    model.cuda()

  model.eval()
  model.volatile(True) # custom method for setting variables to volatile for eval.

  num_data = len(X[0])
  batch_size = 32

  results = []

  # evaluate data in batches.
  for batch in range(num_data // batch_size + 1):

    # get batch.
    X_batch = get_process_batch(X, num_data, batch_size, batch=batch)

    # step scheduler.
    y_batch = model(X_batch)

    # stash results
    results.append(y_batch.cpu().data)

  # join results.
  if len(results) < 1:
    out =  results[0]
  else:
    out = torch.cat(results, 0)

  return out.numpy().tolist()

def get_process_batch(X: List[List[str]], num_train: int, batch_size: int, batch: int=None) -> Tuple[Any, Any]:
  r"""Fetches a batch out of the data. For now no dataloader is necessary.

  Args:
    X (zipped(list(string), list(string))): data.
    num_train (int): The number of training points.
    batch_size (int): The size of the batch.
    batch (int): Index of the batch. If true, does not randomize input.
  Returns:
    X_batch (zipped(list(string), list(string), list(string))): Batched training data.
  """
  C, E = X
  start = batch_size*batch
  BC, BE =  C[start:start+batch_size], E[start:start+batch_size]
  X_batch = BC, BE
  return X_batch