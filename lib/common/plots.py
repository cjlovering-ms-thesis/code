import matplotlib.pyplot as plt
import numpy as np

import datetime

from itertools import cycle
from .save import to_title

def plot_history(histories, group_title, record_val=50, save_plots=False, save_name=""):
  """Plots history of training.

  Example::

    history = {
      'train': {
        'loss': [],
        'auc': [],
        'acc': [],
      },
      'val': {
        'loss': [],
        'auc': [],
        'acc': [],
      }
    }
    plot_histories([history], 'example')

  """
  plt.figure(figsize=(10.5,3.5))
  middle_ax = None
  for idx, key in enumerate([ 'loss' ]):
    colors = cycle(["b", "g", "r", "c", "k", "m", "y"])
    plot_idx = idx + 1
    ax = plt.subplot(1, 3, plot_idx)

    if plot_idx == 2:
      middle_ax = ax

    plt.xlabel('Epochs')
    plt.ylabel(key)

    for history, hyperparams in histories:
      title = to_title(hyperparams)
      x = np.arange(len(history['val'][key])) * record_val
      color = next(colors)
      plt.plot(x, history['train'][key], '{}--*'.format(color), label="{} - trn".format(title))
      plt.plot(x, history['val'][key], '{}-d'.format(color), label="{} - val".format(title))

  box = middle_ax.get_position()
  middle_ax.legend(loc='upper center', bbox_to_anchor=(-0.45, -0.25),
          fancybox=False, shadow=False, ncol=1)
  plt.suptitle(group_title)
  if save_plots:
    now = datetime.datetime.now().strftime("%d-%H-%M-%S")
    plt.savefig('../checkpoints/plots/{}-{}.svg')
  else:
    plt.show()

def plot_histories(histories, group_title, record_val=50, save_plots=False, save_name=""):
  """Plots history of training.

  Example::

    history = {
      'train': {
        'loss': [],
        'auc': [],
        'acc': [],
      },
      'val': {
        'loss': [],
        'auc': [],
        'acc': [],
      }
    }
    plot_histories([history], 'example')

  """


  total = 0
  for key in [ 'acc', 'loss', 'auc' ]:
    if key not in histories[0][0]['train']:
      continue
    total +=1

  if total == 3:
    plt.figure(figsize=(10.5,3.5))
  else: 
    plt.figure(figsize=(2.5,3.5))
  middle_ax = None

  for idx, key in enumerate([ 'acc', 'loss', 'auc' ]):
    if key not in histories[0][0]['train']:
      continue
    
    colors = cycle(["b", "g", "r", "c", "k", "m", "y"])
    plot_idx = min(idx + 1, total)
    ax = plt.subplot(1, total, plot_idx)

    if total == 3 and plot_idx == 2:
      middle_ax = ax
    elif total != 3:
      middle_ax = ax

    plt.xlabel('Epochs')
    plt.ylabel(key)

    for history, hyperparams in histories:
      title = to_title(hyperparams)
      x = np.arange(len(history['val'][key])) * record_val
      color = next(colors)
      plt.plot(x, history['train'][key], '{}--*'.format(color), label="{} - trn".format(title))
      plt.plot(x, history['val'][key], '{}-d'.format(color), label="{} - val".format(title))

  box = middle_ax.get_position()
  middle_ax.legend(loc='upper center', bbox_to_anchor=(-0.45, -0.25),
          fancybox=False, shadow=False, ncol=1)
  plt.suptitle(group_title)
  if save_plots:
    now = datetime.datetime.now().strftime("%d-%H-%M-%S")
    plt.savefig('../checkpoints/plots/{}-{}.svg')
  else:
    plt.show()

def plot_histories_vertical(histories, group_title, record_val=1, save_plots=False, save_name=""):
  """Plots history of training.

  Example::

    history = {
      'train': {
        'loss': [],
        'auc': [],
        'acc': [],
      },
      'val': {
        'loss': [],
        'auc': [],
        'acc': [],
      }
    }
    plot_histories([history], 'example')

  """

  total = 0
  for key in [ 'acc', 'loss', 'auc' ]:
    if key not in histories[0][0]['train']:
      continue
    total +=1
  if total == 3:
    plt.figure(figsize=(10.5,10.5))
  if total == 2:
    plt.figure(figsize=(10.5,7.0))
  if total == 1:
    plt.figure(figsize=(10.5,3.5))
  middle_ax = None

  for idx, key in enumerate([ 'acc', 'loss', 'auc' ]):
    if key not in histories[0][0]['train']:
      continue
    colors = cycle(["b", "g", "r", "c", "k", "m", "y"])
    plot_idx = min(idx + 1, total)
    ax = plt.subplot(total, 1, plot_idx)

    if total == 3 and plot_idx == 2:
      middle_ax = ax
    elif total != 3:
      middle_ax = ax

    plt.xlabel('Epochs')
    plt.ylabel(key)

    for history, hyperparams in histories:
      title = to_title(hyperparams)
      x = np.arange(len(history['val'][key])) * record_val
      color = next(colors)
      plt.plot(x, history['train'][key], '{}--*'.format(color), label="{} - trn".format(title))
      plt.plot(x, history['val'][key], '{}-d'.format(color), label="{} - val".format(title))

  box = middle_ax.get_position()
  middle_ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.25),
          fancybox=False, shadow=False, ncol=1)
  plt.tight_layout()
  plt.subplots_adjust(top=0.90)
  plt.suptitle(group_title)
  if save_plots:
    now = datetime.datetime.now().strftime("%d-%H-%M-%S")
    plt.savefig('../checkpoints/plots/{}-{}.svg')
  else:
    plt.show()
