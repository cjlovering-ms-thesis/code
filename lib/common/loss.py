import torch
import numpy as np
from typing import List
from torch.autograd import Variable
from sklearn.metrics import roc_auc_score

def document_pairwise_loss(scores: Variable, idx_left: List[int], idx_right: List[int],
     y_true: torch.FloatTensor, loss: torch.nn.Module=None, cuda: bool=True) -> Variable:
  r"""Compute total loss across a document of sparse labels.

  Args:
      scores (torch.Variable) ∈ (N,1): List of scores where scores_i maps directly to sentence in \
          current document.
      idx_left (list(int)) ∈ (N,1): List of integer indices of sentences compared.
      idx_right (list(int)) ∈ (N,1): List of integer indices of sentences compared.
      y_true (torch.Variable) ∈ (N,1): true labels that are either 0 or 1
  Returns:
      average pairwise loss across all the labeled pairs.
  """
  y_true = Variable(torch.FloatTensor(y_true), requires_grad=False)
  y_0_torch, y_1_torch = torch.LongTensor(idx_left), torch.LongTensor(idx_right)
  if torch.cuda.is_available() and cuda:
    y_true, y_0_torch, y_1_torch = y_true.cuda(), y_0_torch.cuda(), y_1_torch.cuda()
  y_0, y_1 = scores[y_0_torch], scores[y_1_torch]
  return pairwise_loss_ranking(y_0, y_1, y_true, margin=1, loss=loss)

def document_pairwise_acc(scores: Variable, idx_left: List[int], idx_right: List[int],
     y_true: torch.FloatTensor, cuda: bool=True) -> Variable:
  r"""Compute total loss across a document of sparse labels.

  Args:
      scores (torch.Variable) ∈ (N,1): List of scores where scores_i maps directly to sentence in \
          current document.
      idx_left (list(int)) ∈ (N,1): List of integer indices of sentences compared.
      idx_right (list(int)) ∈ (N,1): List of integer indices of sentences compared.
      y_true (torch.Variable) ∈ (N,1): true labels that are either 0 or 1
  Returns:
      accuracy across all the labeled pairs.
  """
  y_true = Variable(torch.FloatTensor(y_true), requires_grad=False)
  y_0_torch, y_1_torch = torch.LongTensor(idx_left), torch.LongTensor(idx_right)
  if torch.cuda.is_available() and cuda:
    y_true, y_0_torch, y_1_torch = y_true.cuda(), y_0_torch.cuda(), y_1_torch.cuda()
  y_0, y_1 = scores[y_0_torch], scores[y_1_torch]
  return pairwise_acc(y_0, y_1, y_true)

def pairwise_loss(y_0: Variable, y_1: Variable, y_true: Variable, margin: int=1) -> Variable:
  r"""Compute pairwise loss on a set of scores.

  Args:
    y_0 (Variable) ∈ (N,1): a set of scores.
    y_1 (Variable) ∈ (N,1): a set of scores.
    y_true (Variable) ∈ (N): true labels that are either 0 or 1.
  Returns:
    (float): the pairwise loss.
  """
  y_inv = y_true==0
  _0_correct = (y_0 - y_1 + margin).squeeze().mul(y_true.float()).clamp(min=0)
  _1_correct = (y_1 - y_0 + margin).squeeze().mul(y_inv.float()).clamp(min=0)
  return torch.mean(_0_correct + _1_correct)

def pairwise_loss_ranking(y_0: Variable, y_1: Variable, y_true: torch.FloatTensor,
    margin: int=1, loss: torch.nn.Module=None) -> Variable:
  r"""Compute pairwise loss on a set of scores. Built on a pytorch MarginRankingLoss.

  Args:
    y_0 (Variable) ∈ (N,1): a set of scores.
    y_1 (Variable) ∈ (N,1): a set of scores.
    y_true (Variable) ∈ (N): true labels that are either 0 or 1.
    margin (int): the margin between labels.
    loss (torch.nn.Module): If not None, pre-initialized instantiate loss function.
  Returns:
    (Variable): the pairwise loss.
  """
  if loss is None:
    loss = torch.nn.MarginRankingLoss(margin=margin)
  y_gen = y_true.clone()
  y_gen[y_true==0] = 1
  y_gen[y_true==1] = -1
  return loss(y_0.squeeze().float(), y_1.squeeze().float(), y_gen.squeeze().float())

def pairwise_acc(y_0: Variable, y_1: Variable, y_true: Variable) -> Variable:
  r"""Determine the percentage of items scored correctly.

  Args:
    y_0 (Variable) ∈ (N,1): a set of scores.
    y_1 (Variable) ∈ (N,1): a set of scores.
    y_true (Variable) ∈ (N): true labels that are either 0 or 1.
  Returns:
    (float): the accuracy.

  .. note::
    In order to make our loss/metric API consistent, we expect Variables
    and return a Variable, even though it is not necessary especially for
    accuracy and area under the roc curve.
  """
  predictions = y_0 <= y_1
  correct = torch.eq(predictions.squeeze(), y_true.byte().squeeze()).float()
  return torch.mean(correct)

""" tests below """

def test_document_pairwise_acc():
  scores = Variable(torch.FloatTensor([0, 1, 2]))
  labels = (
    [0, 1],
    [1, 2],
    Variable(torch.FloatTensor([1, 1]))
  )
  assert document_pairwise_loss(scores, *labels).data[0] == 1.

def test_document_pairwise_loss():
  scores = Variable(torch.FloatTensor([0, 1, 2]))
  labels = (
    [0, 1],
    [1, 2],
    Variable(torch.FloatTensor([1, 1]))
  )
  assert document_pairwise_loss(scores, *labels).data[0] == 0.

  labels = (
    [0, 1],
    [1, 2],
    Variable(torch.FloatTensor([0, 0]))
  )
  assert document_pairwise_loss(scores, *labels).data[0] == 2.

  labels = (
    [1, 2],
    [0, 1],
    Variable(torch.FloatTensor([1, 1]))
  )
  assert document_pairwise_loss(scores, *labels).data[0] == 2.

  labels = (
    [1, 2],
    [0, 1],
    Variable(torch.FloatTensor([0, 0]))
  )
  assert document_pairwise_loss(scores, *labels).data[0] == 0.

def test_pairwise_ranking_loss_base():
  y_0 = Variable(torch.FloatTensor([1]))
  y_1 = Variable(torch.FloatTensor([0]))
  y = Variable(torch.zeros(1))
  assert pairwise_loss_ranking(y_0, y_1, y).data[0] == 0.
  assert pairwise_loss_ranking(y_1, y_0, y).data[0] == 2.
  assert pairwise_loss_ranking(y_0, y_1, y, margin=1).data[0] == 0.
  assert pairwise_loss_ranking(y_0, y_1, y, margin=2).data[0] == 1.

def test_pairwise_acc():
  y_0 = Variable(torch.FloatTensor([0, 1, 2]))
  y_1 = Variable(torch.FloatTensor([1, 2, 3]))
  y = Variable(torch.ones(3).float())
  assert pairwise_acc(y_0, y_1, y).data[0] == 1.

  y = Variable(torch.zeros(3).float())
  assert pairwise_acc(y_0, y_1, y).data[0] == 0.

  y = Variable(torch.zeros(3).float())
  assert pairwise_acc(y_1, y_0, y).data[0] == 1.

  y = Variable(torch.ones(3).float())
  assert pairwise_acc(y_1, y_0, y).data[0] == 0.

def test_pairwise_loss_base():
  y_0 = Variable(torch.FloatTensor([1, 2, 3]))
  y_1 = Variable(torch.FloatTensor([1, 2, 3]))
  y = Variable(torch.ones(3))
  assert pairwise_loss(y_0, y_1, y).data[0] == 1

def test_pairwise_loss_squeeze():
  y_0 = Variable(torch.FloatTensor([[1, 2, 3]]))
  y_1 = Variable(torch.FloatTensor([1, 2, 3]))
  y = Variable(torch.ones(3))
  assert pairwise_loss(y_0, y_1, y).data[0] == 1

def test_pairwise_loss_correct():
  y_0 = Variable(torch.FloatTensor([[3, 1, 1]]))
  y_1 = Variable(torch.FloatTensor([1, 3, 3]))
  y = Variable(torch.FloatTensor([0, 1, 1]))
  assert pairwise_loss(y_0, y_1, y).data[0] == 0

  y_0 = Variable(torch.FloatTensor([[3, 1, 1]]))
  y_1 = Variable(torch.FloatTensor([1, 3, 3]))
  y = Variable(torch.FloatTensor([1, 0, 0]))
  assert pairwise_loss(y_0, y_1, y).data[0] == 3

def test_pairwise_loss_clamp():
  y_0 = Variable(torch.FloatTensor([3]))
  y_1 = Variable(torch.FloatTensor([0]))
  y = Variable(torch.FloatTensor([0]), requires_grad=False)
  assert pairwise_loss(y_0, y_1, y).data[0] == 0
