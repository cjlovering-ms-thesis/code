import torch
import gc

import numpy as np

from collections import defaultdict
from torch.autograd import Variable
from typing import Any, Dict, List, Tuple

from .loss import document_pairwise_acc, document_pairwise_loss

def train(train_data, val_data, lr=1e-3, reg=0, optim=torch.optim.SGD, model_class=None, config=None, epochs=50, record_val=5, title=None, cuda: bool=False):
  r"""Training function that has the primary responsibility of batching data & organizing epochs.

  Args:
    X (list(list(string)) | X(list(list(ndarray))): The training data.
    y (ndarray): The training labels.
    X_val (list(list(string)) | X(list(list(ndarray))): The validation data.
    y_val (ndarray): The validation labels.
    lr (float): The learning rate. TODO: allow for lr schedules.
    reg (float): Weight decay.
    optim (torch.optim): Optimizer/learner.
    model_class (torch.nn.Module): The NN class to train.
    config (object): Parameter pairs for the given model.
    epochs (int): Number of epochs to train.
    record_val (int): Store history every record_val epoch. User `-1` to turn this off.
  Returns:
    model (torch.Optim): Returns the trained model.
    history (object): Contains the history of the training.
  """

  # set paramaters
  outputs = 1
  batch_size = 32

  # construct our model by instantiating the class defined above.
  model = model_class(**config)

  # put model on gpu if cuda is available.
  if torch.cuda.is_available() and cuda:
    model = model.cuda()

  # construct our loss function w/ optimizer
  # model.parameters() contains the learnable parameters of the model
  if lr >= 0:
    optimizer = optim(model.parameters(), lr=lr, weight_decay=reg)
  else:
    # use defaults
    optimizer = optim(model.parameters(), weight_decay=reg)

  scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, patience=2)

  history = {
    'train': { 'loss': [], 'acc': [] },
    'val':   { 'loss': [], 'acc': [] },
  }

  ranking_loss = torch.nn.MarginRankingLoss(margin=1)

  # in each epoch, we traverse through all documents.
  for t in range(epochs):
    model.train()
    model.volatile(False)

    train_acc_list = []
    train_loss_list = []

    # we operate on each document via batches.
    for doc_rep in train_data:

      X_doc = doc_rep['X']
      y_doc = doc_rep['y']
      c = doc_rep['claim']
      idx_left, idx_right, correct = zip(*y_doc)
      doc_size = len(X_doc)

      y_hat_l = []

      # batch across a document (some documents are large.)
      for batch in range(doc_size // batch_size + (doc_size % batch_size > 0)):

        # fetch batch
        X_batch = get_batch(X_doc, batch_size, batch)

        # forward pass outputting scores
        data = c, X_batch
        y_hat = model(data)
        
        # stash results
        y_hat_l.append(y_hat.cpu())

      # join scores
      if len(y_hat_l) > 1:
        y_hat = torch.cat(y_hat_l, 0)
      else:
        y_hat = y_hat_l[0]

      # compute loss
      loss = document_pairwise_loss(y_hat, idx_left, idx_right, correct, loss=ranking_loss, cuda=cuda)
      acc = document_pairwise_acc(y_hat, idx_left, idx_right, correct, cuda=cuda)

      train_acc_list.append(acc.data[0])
      train_loss_list.append(loss.data[0])

      # zero gradients, perform a backward pass, and update the weights.
      optimizer.zero_grad()
      loss.backward()
      optimizer.step()

      del loss
      gc.collect()

    # train results
    train_acc = torch.mean(torch.FloatTensor(train_acc_list))
    train_loss = torch.mean(torch.FloatTensor(train_loss_list))

    # sample validation data to keep size small.
    doc_scores_val = []
    model.eval()
    model.volatile(True) # custom method for setting variables to volatile for eval.

    val_acc_list = []
    val_loss_list = []

    # speed up 
    if t != (epochs - 1):
      continue

    # we operate on each document via batches.
    for doc_rep in val_data:

      X_doc = doc_rep['X']
      y_doc = doc_rep['y']
      idx_left, idx_right, correct = zip(*y_doc)
      c = doc_rep['claim']
      doc_size = len(X_doc)

      # forward pass outputting scores
      y_hat_l = []

      # batch across a document (some documents are large.)
      for batch in range(doc_size // batch_size + (doc_size % batch_size > 0)):

        # fetch batch
        X_batch = get_batch(X_doc, batch_size, batch)

        # forward pass outputting scores
        data = c, X_batch
        y_hat = model(data)
        
        # stash results
        y_hat_l.append(y_hat.cpu())

      # join scores
      if len(y_hat_l) > 1:
        y_hat = torch.cat(y_hat_l, 0)
      else:
        y_hat = y_hat_l[0]

      # compute loss
      loss = document_pairwise_loss(y_hat, idx_left, idx_right, correct, loss=ranking_loss, cuda=cuda)
      acc = document_pairwise_acc(y_hat, idx_left, idx_right, correct, cuda=cuda)

      val_acc_list.append(acc.data[0])
      val_loss_list.append(loss.data[0])

    # join results
    val_acc = torch.mean(torch.FloatTensor(val_acc_list))
    val_loss = torch.mean(torch.FloatTensor(val_loss_list))
    print("val - final: {}".format(val_acc))

    scheduler.step(val_loss)

    # record values
    if record_val > 0 and t % record_val == 0:
      history = record(history, val_acc, train_acc, val_loss, train_loss)

    # stopping condition based on scheduler
    if optimizer.state_dict()['param_groups'][-1]['lr'] < 1e-9:
      print('Learning is finished after {} epochs.'.format(t))
      break

    # clean up to reduce memory usage: i.e. leave no hanging variables.
    del val_acc_list[:], val_loss_list[:], train_acc_list[:], train_loss_list[:]
    gc.collect()

  return model, history

def get_batch(X: List[str], batch_size: int, batch: int) -> List[str]:
  r"""Fetches a batch out of the data. For now no dataloader is necessary.

  Args:
    X (List[str]): Training data.
    batch_size (int): The size of the batch.
    batch (int): Index of the batch. If true, does not randomize input.
  Returns:
    X_batch (zipped(list(string), list(string), list(string))): Batched training data.
  """
  start = batch_size*batch
  X_batch = X[start:start+batch_size]
  return X[start:start+batch_size]

def record(history: Dict, val_acc: float, train_acc: float, val_loss: float, train_loss: float) -> Dict:
  r""" Operates on validation data & stores training results. """
  history['val']['loss'].append(val_loss)
  history['train']['loss'].append(train_loss)
  history['val']['acc'].append(val_acc)
  history['train']['acc'].append(train_acc)
  return history
