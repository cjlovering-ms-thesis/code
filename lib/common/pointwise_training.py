import torch
import gc

import numpy as np

from collections import defaultdict
from torch.autograd import Variable
from typing import Any, Dict, List, Tuple

from .loss import  pairwise_acc, pairwise_loss, pairwise_loss_ranking

def train(X, y, X_val, y_val, lr=1e-3, reg=0, optim=torch.optim.SGD, model_class=None, config=None, epochs=50, record_val=5, title=None):
  r"""Training function that has the primary responsibility of batching data & organizing epochs.
  
  Args:
    X (list(list(string)) | X(list(list(ndarray))): The training data.
    y (ndarray): The training labels.
    X_val (list(list(string)) | X(list(list(ndarray))): The validation data.
    y_val (ndarray): The validation labels.
    lr (float): The learning rate. TODO: allow for lr schedules.
    reg (float): Weight decay.
    optim (torch.optim): Optimizer/learner.
    model_class (torch.nn.Module): The NN class to train.
    config (object): Parameter pairs for the given model.
    epochs (int): Number of epochs to train. 
    record_val (int): Store history every record_val epoch. User `-1` to turn this off.
  Returns:
    model (torch.Optim): Returns the trained model.
    history (object): Contains the history of the training.
  """
  # data formating. 
  y_val = Variable(torch.from_numpy(np.array(y_val)), requires_grad=False).float()
  if torch.cuda.is_available():
    y_val = y_val.cuda()
  
  # set paramaters.
  num_val = len(X_val[0])
  num_train = len(X[0])
  outputs = 1
  batch_size = 64

  # construct our model by instantiating the class defined above.
  model = model_class(**config)
  
  # put model on gpu if cuda is available.
  if torch.cuda.is_available():
    model = model.cuda()
  
  # construct our loss function w/ optimizer
  # model.parameters() contains the learnable parameters of the model
  if lr >= 0:
    optimizer = optim(model.parameters(), lr=lr, weight_decay=reg)
  else:
    # use defaults
    optimizer = optim(model.parameters(), weight_decay=reg)

  scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, patience=2)
  
  history = { 
    'train': { 'loss': [] }, 
    'val':   { 'loss': [] },
  }

  mse = torch.nn.MSELoss()

  for t in range(epochs):
    model.train()
    model.volatile(False)
    for batch in range(num_train // batch_size):

      # fetch batch
      X_batch, y_batch = get_batch(X, y, num_train, batch_size)

      # forward pass outputting scores
      y_hat = model(X_batch)

      # compute loss
      loss = mse(y_hat, y_batch)

      # zero gradients, perform a backward pass, and update the weights.
      optimizer.zero_grad()
      loss.backward()
      optimizer.step()

    # sample validation data to keep size small.
    y_val_hat_l, y_val_l = [], []
    model.eval()
    model.volatile(True) # custom method for setting variables to volatile for eval.

    for batch in range(num_val // batch_size + 1):
      X_val_sample_batch, y_val_sample_batch = get_batch(X, y, num_val, batch_size, batch=batch)
      y_val_hat = model(X_val_sample_batch)

      # stash results
      y_val_hat_l.append(y_val_hat)
      y_val_l.append(y_val_sample_batch)

    if len(y_val_hat_l) > 1:
      val_loss = mse(torch.cat(y_val_l, 0), torch.cat(y_val_hat_l, 0)).data[0]
    else: 
      val_loss = mse(y_val_l[0], y_val_hat_l[0]).data[0]
    scheduler.step(val_loss)

    # record values
    if record_val > 0 and t % record_val == 0: 
      history = record(model, history, val_loss, loss.data[0])

    # stopping condition based on scheduler
    if optimizer.state_dict()['param_groups'][-1]['lr'] < 1e-9:
      print('Learning is finished after {} epochs.'.format(t))
      break

    # clean up to reduce memory usage: i.e. leave no hanging variables.
    del y_hat, y_val_hat, y_batch, y_val_hat_l[:], y_val_l[:], val_loss
    gc.collect()

  return model, history

def get_batch(X: List[List[str]], y: List[int], num_train: int, batch_size: int, batch: int=None) -> Tuple[Any, Any]:
  r"""Fetches a batch out of the data. For now no dataloader is necessary.

  Args:
    X (zipped(list(string), list(string), list(string))): Training data.
    y (list(int)): The training labels.
    num_train (int): The number of training points.
    batch_size (int): The size of the batch.
    batch (int): Index of the batch. If true, does not randomize input.
  Returns:
    X_batch (zipped(list(string), list(string), list(string))): Batched training data.
    y_batch (Variable): Batched labels.
  """
  def get(sentences: List, mask) -> List:
    return [ sentences[i] for i in mask ]
  C, E = X
  mask = np.random.choice(num_train, batch_size)

  if batch is None:
    BC, BE =  get(C, mask), get(E, mask)
    By = np.array(y)[mask]
  else:
    start = batch_size*batch
    BC, BE =  C[start:start+batch_size], E[start:start+batch_size]
    By = y[start:start+batch_size]

  X_batch = BC, BE
  y_batch = Variable(torch.from_numpy(np.array(By)), requires_grad=False).float()
  if torch.cuda.is_available():
    y_batch = y_batch.cuda()
  return X_batch, y_batch

def record(model: torch.nn.Module, history: Dict, val_loss: float, train_loss: float) -> Dict:
  r""" Operates on validation data & stores training results. """
  history['val']['loss'].append(val_loss)
  history['train']['loss'].append(train_loss)
  return history
