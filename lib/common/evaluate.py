import json
import torch

import numpy as np

from typing import Any, Dict, List, Tuple

from .loss import  pairwise_acc, pairwise_loss, pairwise_loss_ranking
from .training import get_batch

def evaluate(X: List[List[str]], y: List[int], model_path: str=None, model_class: torch.nn.Module=None, 
      args: Dict[str, Any]={}, verbose: bool=True) -> Tuple[float, float, float]:
  """Evalutes saved model on the given X and y.
  """
  if model_class is not None:
    model = model_class(**args)
    model.load_state_dict(torch.load(model_path))
  else:
    model = torch.load(model_path)

  if torch.cuda.is_available():
    model.cuda()

  model.eval()
  model.volatile(True) # custom method for setting variables to volatile for eval.

  num_data = len(y)
  batch_size = 32

  y_1_l, y_2_l, y_l = [], [], []

  # evaluate data in batches.
  for batch in range(num_data // (batch_size + 1)):
    # get batches.
    X_batch, y_batch = get_batch(X, y, num_data, batch_size, batch=batch)
    C_batch, E1_batch, E2_batch = X_batch
    
    # get samples.
    X1_sample = C_batch, E1_batch
    X2_sample = C_batch, E2_batch

    # step scheduler.        
    y_1 = model(X1_sample)
    y_2 = model(X2_sample)

    # stash results
    y_1_l.append(y_1.cpu().data)
    y_2_l.append(y_2.cpu().data) 
    y_l.append(y_batch.cpu().data)

  # join results.
  y_1, y_2, y_batch = torch.cat(y_1_l, 0), torch.cat(y_2_l, 0), torch.cat(y_l, 0)

  # compute metrics
  loss = pairwise_loss_ranking(y_1, y_2, y_batch)
  acc = pairwise_acc(y_1, y_2, y_batch)

  if verbose:
    print("loss: {}, accuracy: {}".format(loss, acc))
  return loss.data[0], acc, auc.data[0]

def error_analysis(X: List[List[str]], y: List[int], model_path: str=None, model_class: torch.nn.Module=None, 
      args: Dict[str, Any]={}, verbose: bool=True) -> Tuple[float, float, float]:
  """Evalutes saved model on the given X and y.
  """
  if model_class is not None:
    model = model_class(**args)
    model.load_state_dict(torch.load(model_path))
  else:
    model = torch.load(model_path)

  if torch.cuda.is_available():
    model.cuda()

  model.eval()
  model.volatile(True) # custom method for setting variables to volatile for eval.

  num_data = len(y)
  batch_size = 32

  y_1_l, y_2_l, y_l = [], [], []

  # evaluate data in batches.
  for batch in range(num_data // (batch_size + 1)):
    # get batches.
    X_batch, y_batch = get_batch(X, y, num_data, batch_size, batch=batch)
    C_batch, E1_batch, E2_batch = X_batch
    
    # get samples.
    X1_sample = C_batch, E1_batch
    X2_sample = C_batch, E2_batch

    # step scheduler.        
    y_1 = model(X1_sample)
    y_2 = model(X2_sample)

    # stash results
    y_1_l.append(y_1.cpu().data)
    y_2_l.append(y_2.cpu().data) 
    y_l.append(y_batch.cpu().data)

  # join results.
  y_1, y_2, y_batch = torch.cat(y_1_l, 0), torch.cat(y_2_l, 0), torch.cat(y_l, 0)

  # compute metrics.
  loss = pairwise_loss_ranking(y_1, y_2, y_batch)
  acc = pairwise_acc(y_1, y_2, y_batch)
  auc = pairwise_auc(y_1, y_2, y_batch)

  # compute statistics.
  differences = torch.abs(y_1 - y_2).numpy()
  scores = torch.cat([y_1, y_2], 1).numpy()

  score_mean = np.mean(scores)
  score_var = np.var(scores)
  score_max = np.max(scores)
  score_min = np.min(scores)

  score_diff_mean = np.mean(differences)
  score_diff_var = np.var(differences)
  score_diff_max = np.max(differences)
  score_diff_min = np.min(differences)

  return loss.data[0], acc, auc.data[0], score_mean, score_var, score_max, score_min, score_diff_mean, score_diff_var, score_diff_max, score_diff_min
