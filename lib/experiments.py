"""This script is used to evaluate our models on increasing amounts of data. """
import argparse
import numpy as np
import json
import csv
import json
import uuid
import torch

import pandas as pd
import matplotlib.pyplot as plt

##

import logging

##

from modules.highway import Highway
from modules.attention import Attention
from modules.similarity_matrix import SimilarityMatrix

import torch
import torchtext.vocab as vocab
import numpy as np

from torch.autograd import Variable

class BiDafR(torch.nn.Module):
  """BiDaf-r: this model shares a recurrent network for the claim and the evidence. """
  def __init__(self, word_emb_dim: int=200, hidden_size: int=100, out_dim: int=1, emb_vocab=vocab.GloVe,
      encoder: torch.nn.Module=torch.nn.LSTM, num_enc_layers: int=1, bidirectional: bool=False, dropout: float=0.,
      highway_layers_wrd: int=0, highway_layers_seq: int=0, attend_claim: bool=False, 
      attend_evidence: bool=False) -> None:
    """Initialize all layers.

    Args:
        word_emb_dim (int): size of word embedding.
        hidden_size (int): size of hidden state of recurrent networks.
        out_dim (int): the number of out dim (for us always 1).
        emb_vocab (vocab): the type of vocab (Glove vs word2vec).
        rec_net (torch.nn): the type of recurrent network (RNN, Glove, word2vec).
        bidirectional (bool): if true recurrent networks are bi-directional.
        attention (bool): if true add attention
    """
    super(BiDafR, self).__init__()
    self.word_emb_dim = word_emb_dim
    self.vocab = emb_vocab(name='6B', dim=self.word_emb_dim)
    self._volatile = False
    
    # if highway init highway layers for words
    self.highway_wrd = highway_layers_wrd > 0
    if self.highway_wrd:
      self.highway_wrd_network = Highway(input_size=word_emb_dim, num_layers=highway_layers_wrd)

    # determine how big final layer is.
    context_size = hidden_size * 3
    similarity_size = hidden_size

    # if bidirectional, reduce hidden size by x2 to keep # of params.
    hidden_size = hidden_size // 2 if bidirectional else hidden_size

    # sentence context encoders
    self.c_seq_encoder = encoder(input_size=self.word_emb_dim, hidden_size=hidden_size, \
        num_layers=num_enc_layers, bidirectional=bidirectional, dropout=dropout)
    self.e_seq_encoder = encoder(input_size=self.word_emb_dim, hidden_size=hidden_size, \
        num_layers=num_enc_layers, bidirectional=bidirectional, dropout=dropout)

    # attention
    self.attend_claim = attend_claim
    self.attend_evidence = attend_evidence
    if attend_claim or attend_evidence:
      self.similarity_matrix = SimilarityMatrix(hidden_size=similarity_size)
      self.attend = Attention()

    # if highway init highway layers for seq representation
    self.highway_seq = highway_layers_seq > 0
    if self.highway_seq:
      self.highway_seq_network = Highway(input_size=context_size, num_layers=highway_layers_seq)

    self.final_affine = torch.nn.Linear(context_size, out_dim, bias=False)

  def forward(self, x):
    """Forward pass on input sentences, predicting a score for each pair of claim & evidence.

    Args:
        x - List(List(string)): Each string is a sentence.
    """
    # split into claim and evidence.
    c, e = x

    # embed all words using chosen embedding (default: GloVe).
    c = self.get_batch(self.prepare_batch(c))
    e = self.get_batch(self.prepare_batch(e))

    # setup highway for encoding words.
    if self.highway_wrd:
      c = self.highway_wrd_network(c)
      e = self.highway_wrd_network(e)

    # process sentences through a sentence encoder (default: GRU).
    c_u, _ = self.c_seq_encoder(c)
    e_u, _ = self.e_seq_encoder(e)

    features = []

    if self.attend_claim or self.attend_evidence:
      mtx = self.similarity_matrix(c_u, e_u)

    if self.attend_claim:
      c_a = self.attend(c_u, mtx)
      features.append(c_a)
    else:
      c_u_l = c_u[:,-1,:]
      features.append(c_u_l)

    if self.attend_evidence:
      e_a = self.attend(e_u, mtx)
      features.append(e_a)
    else:
      e_u_l = e_u[:,-1,:]
      features.append(e_u_l)

    features = torch.cat([*features, torch.mul(*features)], 1)

    if self.highway_seq:
      features = self.highway_seq_network(features)

    return self.final_affine(features)

  def prepare_batch(self, x):
    """ Args: x - [string, ... ] """
    split = [ s.split() for s in x ]
    filtered = [
        [ w for w in s if w in self.vocab.stoi ]
        for s in x
    ]
    MAX_SIZE = 128
    shortened = [
        s[:MAX_SIZE] for s in filtered
    ]
    return shortened

  def get_batch(self, batch):
    """Embeds a batch of sentences.

    Returns:
      (Variable): Embedded word matrix.
      (IntTensor): Lengths of each sentence.
    """

    batch_size = len(batch)
    max_len = max([len(s) for s in batch])
    embed = torch.zeros(batch_size, max_len, self.word_emb_dim)

    for i in range(batch_size):
      for j in range(len(batch[i])):
        embed[i, j, :] = self.get_word(batch[i][j])

    if torch.cuda.is_available():
        return Variable(torch.FloatTensor(embed), volatile=self._volatile).cuda()
    return Variable(torch.FloatTensor(embed), volatile=self._volatile)

  def get_word(self, word):
    """Returns the embedding for a word. """
    if word not in self.vocab.stoi:
        return self.vocab.vectors[0]
    return self.vocab.vectors[self.vocab.stoi[word]]

  def volatile(self, volatile):
    """Sets state to generate volatile vecotors. """
    self._volatile = volatile

##

from torch.autograd import Variable
from common.training import train
from utils.data_io import get_data

def sample(X,k):
    a,b,c = X
    return (a[:k], b[:k], c[:k])

MAX_TRN_COUNT = 35000
MAX_VAL_COUNT = 10000

EPOCHS_SMALL_DS = 15
RECORD_SMALL_DS = 1

from collections import defaultdict
results = defaultdict(list)

def sample(X, y, k=25,z=0):
    a,b,c = X
    return (a[z:z+k], b[z:z+k], c[z:z+k]), y[z:z+k]

def shuffle(X, y):
  def get(sentences, mask):
    return [ sentences[i] for i in mask ]

  C, E1, E2 = X
  mask = np.random.choice(len(y), len(y))
  BC, BE1, BE2 =  get(C, mask), get(E1, mask), get(E2, mask)
  By = get(y,mask)
  return (BC, BE1, BE2), By

def main(config):
  
  logging.basicConfig(filename='{}.csv'.format(config.log_file),level=logging.INFO)

  X_train, y_train, X_val, y_val, X_test, y_test = get_data(config.in_path)
  print("train: {}, val: {}, test: {}".format(len(y_train), len(y_val), len(y_test)))

  X_train, y_train = sample(X_train, y_train, MAX_TRN_COUNT)
  X_val, y_val = sample(X_val, y_val, MAX_VAL_COUNT)
  
  print("train: {}, val: {}, test: {}".format(len(y_train), len(y_val), len(y_test)))

  for _ in range(3):

      # shuffle data
      X_shuffle_train, y_shuffle_train = shuffle(X_train, y_train)

      # super sets
      X_0, y_0 = sample(X_shuffle_train, y_shuffle_train,   500, 0)
      X_1, y_1 = sample(X_shuffle_train, y_shuffle_train,  1000, 0)
      X_2, y_2 = sample(X_shuffle_train, y_shuffle_train,  2200, 0)
      X_3, y_3 = sample(X_shuffle_train, y_shuffle_train,  4375, 0)
      X_4, y_4 = sample(X_shuffle_train, y_shuffle_train,  8750, 0)
      X_5, y_5 = sample(X_shuffle_train, y_shuffle_train, 17500, 0)
      X_6, y_6 = sample(X_shuffle_train, y_shuffle_train, 35000, 0)

      data = [
        (X_0, y_0), (X_1, y_1), (X_2, y_2), (X_3, y_3), (X_4, y_4), (X_5, y_5), (X_6, y_6)
      ]

      for i,(X, y) in enumerate(data):
          hyperparams = {
            'config':{
                'bidirectional': True,
                'hidden_size': 200,
                'num_enc_layers': 1,
                'encoder': torch.nn.LSTM,
                'attend_claim': False,
                'attend_evidence': False,
            },
                'optim' : torch.optim.Adam,
                'model_class' : BiDafR,
                'epochs' : EPOCHS_SMALL_DS,
                'record_val' : RECORD_SMALL_DS,
                'batch_size' : 32
          }
          _, history = train(X, y, X_val, y_val, **hyperparams)
          logging.info("{}, {}, {}".format('train', len(y), history['train']['acc']))
          logging.info("{}, {}, {}".format('test', len(y),  history['val']['acc']))
  exit()

if __name__ == "__main__":
  parser = argparse.ArgumentParser()

  # io
  parser.add_argument('--in_path', type=str, default="../data/results/argument/")
  parser.add_argument('--log_file', type=str, default="experiment_results")

  # extract data and vote
  config = parser.parse_args()

  main(config)