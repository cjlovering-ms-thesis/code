import pandas as pd
import numpy as np
from sklearn.metrics import cohen_kappa_score
from itertools import combinations
import functools
import operator

from . import analysis_config
from . import analysis_util

def cohen_scores(raw):
  scores = []
  full = 0
  group_count, trial_count, _ = raw.shape
  for g in range(group_count):
    trials = [
      t for t in range(trial_count) if raw[g][t][0] > -1]
    if len(trials) < 3:
      continue
    else:
      full += 1
    scores.extend([
      cohen_kappa_score(raw[g][i],raw[g][j]) 
      for i,j in combinations(trials, 2)])
  return np.array(scores), full

def best_cohen_scores(raw):
  scores = []
  group_count, trial_count, _ = raw.shape
  for g in range(group_count):
    trials = [
      t for t in range(trial_count) if raw[g][t][0] > -1]
    result = [
      cohen_kappa_score(raw[g][i],raw[g][j]) 
      for i,j in combinations(trials, 2)]
    if result:
      scores.append(np.max(result))
  return np.array(scores)

def analyze(data, experiment_name=""): 
  raw = analysis_util.group_data(data)
  scores, full = cohen_scores(raw)
  best_scores = best_cohen_scores(raw)
  work_time = data['WorkTimeInSeconds']/60.

  # if row is empty, instead return -1 to indicate that there were not enough
  # responses.
  if len(scores) == 0:
    scores = best_scores = [ -1 ]

  # return row of statistics.
  return [
    experiment_name,
    np.max(scores),
    np.min(scores),
    np.mean(scores),
    np.median(scores),
    np.std(scores),
    np.max(best_scores),
    np.min(best_scores),
    np.mean(best_scores),
    np.median(best_scores),
    np.std(best_scores),
    np.max(work_time),
    np.min(work_time),
    np.mean(work_time),
    np.median(work_time),
    np.std(work_time),
    full
  ]

def main(config):
  data = analysis_util.load_annotated(config)
  valid_data = analysis_util.get_valid_data(data)
  invalid_data = analysis_util.get_invalid_data(data)
  results = pd.DataFrame([
      analyze(valid_data, "valid data"),
      analyze(invalid_data, "invalid data"),
      analyze(data, "all data"),
    ],
    columns=[
      'experiment_name', 
      'cohen_score_max', 
      'cohen_score_min', 
      'cohen_score_mean', 
      'cohen_score_med', 
      'cohen_score_std', 
      'best_cohen_score_max', 
      'best_cohen_score_min', 
      'best_cohen_score_mean', 
      'best_cohen_score_med', 
      'best_cohen_score_std', 
      'max_time (m)', 
      'min_time (m)', 
      'mean_time (m)', 
      'med_time (m)', 
      'std_time (m)',
      'hit count']
  )
  results.to_csv(config.data_path + config.analysis_name, index=False)

if __name__ == "__main__":
  config = analysis_config.get_config()
  main(config)