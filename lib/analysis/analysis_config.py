
import argparse

def get_config():
  parser = argparse.ArgumentParser()

  # io
  parser.add_argument('--data_path', type=str, default="../data/output/")
  parser.add_argument('--analysis_name', type=str, default="analysis.csv")
  parser.add_argument('--result_name', type=str, default="results.csv")
  parser.add_argument('--annotated_name', type=str, default="annotated.csv")

  # settings
  parser.add_argument('--multiple_choice_options', type=int, default=4)
  parser.add_argument('--questions_per_task', type=int, default=8)
  parser.add_argument('--gold_per_task', type=int, default=2)

  # extract data and vote
  config = parser.parse_args()

  return config