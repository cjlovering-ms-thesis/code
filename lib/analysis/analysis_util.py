import numpy as np
import pandas as pd

def load_annotated(config):
  """Loads annotated csv file from config. """
  return _load_file(config, config.annotated_name)

def _load_file(config, file_name):
  """Loads file from disk. """
  return pd.read_csv(config.data_path + file_name)

def get_valid_data(data):
  """Gets data that was approved. Data is approved by an external script. """
  condition = data['Approve'] == 'x'
  return data[condition]

def get_invalid_data(data):
  """Gets data that was not approved. Data is approved by an external script. """
  condition = data['Approve'] != 'x'
  return data[condition]

def group_data(data):
  """Groups data by HITId. It strips out gold questions, and returns a 
  tensor (groups,trials,questions) of raw answers.
  """
  groups = data.groupby('HITId').groups.values()
  max_trials = max([len(g) for g in groups])
  raw = np.ones((len(groups),max_trials,8)) * -1
  for j,g in enumerate(groups):
    for r_idx,q_idx in enumerate(g):
      raw[j][r_idx][:] = [ data.loc[q_idx]["Answer.e_{}".format(i)] for i in range(10) if not data.loc[q_idx]["Input.is_gold_{}".format(i)]]
  return raw