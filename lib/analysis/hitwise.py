import pandas as pd
import numpy as np
from sklearn.metrics import cohen_kappa_score
from itertools import combinations
import functools
import operator

from . import analysis_config
from . import analysis_util

def cohen_scores(raw):
  scores = []
  group_count, trial_count, _ = raw.shape
  for g in range(group_count):
    trials = [
      t for t in range(trial_count) if raw[g][t][0] > -1]
    if len(trials) < 3:
      continue
    results = np.array([
      cohen_kappa_score(raw[g][i],raw[g][j]) 
      for i,j in combinations(trials, 2)])
    scores.append([np.mean(results), np.max(results)])
  return scores

def analyze(data, valid: bool, qualified: str):  
  x = pd.DataFrame(
    cohen_scores(group_data(data)),
    columns=[
      'cohen_score_mean', 
      'cohen_score_max'
    ]
  )
  x['valid'] = valid
  x['qualified'] = qualified
  return x

def main(config):
  data = analysis_util.load_annotated(config)
  valid_data = analysis_util.get_valid_data(data)
  invalid_data = analysis_util.get_invalid_data(data)
  x1 = analyze(valid_data, True, config.analysis_name.replace(".csv",""))
  x2 = analyze(invalid_data, False, config.analysis_name.replace(".csv",""))
  d = x1.append(x2)
  d.to_csv(config.data_path + 'hitwise_' + config.analysis_name, index=False)

if __name__ == "__main__":
  config = analysis_config.get_config()
  main(config)