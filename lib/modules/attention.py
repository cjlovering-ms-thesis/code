import torch
from torch.autograd import Variable

class Attention(torch.nn.Module):
  r"""Attention building module. Note, this contains no parameters. """
  def __init__(self) -> None:
    r"""Initialize all layers.
    """
    super(Attention, self).__init__()
    self.softmax = torch.nn.Softmax()

  def forward(self, u: Variable, mtx: Variable, mask: Variable = None) -> Variable:
    r"""Applies highway to variable.
    
    Args:
        u (Variable) ∈ (N,D,H): \
            Hidden state word representations. D is either T or J dependent on the direction. \
            H is the size of the word representations.
        mtx (Variable) ∈ (N,T,J): Similarity matrix variable. Sij = sim(c_i, c_j)
    Returns:
        (Variable) ∈ (N,H): Attended sentence representation.
    """
    s, _ = torch.max(mtx, self.direction(u.size(), mtx.size()))
    alpha = self.softmax(s)
    return torch.sum(torch.mul(alpha.unsqueeze(-1), u),1)

  def direction(self, u_s: torch.Size, mtx_s: torch.Size) -> int:
    r"""Gets the direction of the max operation.

    Args:
      u_s (torch.Size) ∈ (N, D, H): The size of the sentence representation.
      mtx_s (torch.Size) ∈ (N, T, J): The size of the similarity matrix.
    Returns:
      (int): the corresponding direction to max over
    """
    _, D, _ = u_s
    _, _, L = mtx_s
    return 1 if D == L else 2

### tests ###
def test_shape():
  N = 10
  T = 5
  L = 7
  H = 100
  c = Variable(torch.rand(N, T, H))
  e = Variable(torch.rand(N, L, H))
  mtx = Variable(torch.rand(N, T, L))
  a = Attention()
  attended_c = a(c, mtx)
  attended_e = a(e, mtx)
  assert attended_c.size() == torch.Size([N, H])
  assert attended_e.size() == torch.Size([N, H])

