import torch
from torch.autograd import Variable

class AttentionContext(torch.nn.Module):
  r"""Attention building module. Note, this contains no parameters. """
  def __init__(self, input_size: int=80) -> None:
    r"""Initialize all layers.
    """
    super(AttentionContext, self).__init__()
    self.activation = torch.nn.Tanh()
    self.softmax = torch.nn.Softmax()
    self.W = torch.nn.Linear(input_size, input_size)
    self.C = Variable(torch.randn(input_size,1).type(torch.FloatTensor), requires_grad=True)

  def forward(self, x: Variable) -> Variable:
    r"""Applies attention across a vector using claim representation as
    the context vector (used traditionally). We do not normalize attended
    vectors into a single representation, instead we are weighting the 
    vectors.

      u = W_a x + b_a
      alpha = softmax( u^T c )
      u_a = alpha 
    
    Args:
        x (Variable) ∈ (N,H): \
            Hidden state word representations. \
            H is the size of the word representations.
    Returns:
        (Variable) ∈ (N,H): Attended sentence representation.
    """
    # (N,D) dot (D) => (N,)
    u = self.activation(self.W(x))
    # (N,) => (N,)
    alpha = self.softmax(torch.mm(u.squeeze(0), self.C))
    # weight each sentence representation
    return torch.mul(alpha, x)
