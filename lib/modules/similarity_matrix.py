import torch
from torch.autograd import Variable

class SimilarityMatrix(torch.nn.Module):
  def __init__(self, hidden_size: int=40, activation='sigmoid') -> None:
    """Initialize all layers.
    
    Args:
        hidden_size (int): size of hidden state of recurrent networks.
    """
    super(SimilarityMatrix, self).__init__()

    # set join function.    
    self.similarity = torch.nn.Linear(hidden_size*3, 1)
    self.join = lambda c,e: torch.cat([c, e, torch.mul(c, e)], -1)
    
    # set activation function.
    if activation == 'sigmoid':
        self.activation = torch.nn.Sigmoid()
    if activation == 'relu':
        self.activation = torch.nn.ReLU()
    if activation == 'tan':
        self.activation = torch.nn.Tanh()
    if activation == 'identity':
        self.activation = lambda x: x

    # similarity function
    self.similarity_function = lambda c,e: self.activation(self.similarity(self.join(c,e))).squeeze()

  def forward(self, claim: Variable, evidence: Variable) -> Variable:
    """Computes the similarity matrix between two the claim and the evidence.
    
    Args:
        c (Variable) ∈ (N, T, H): \
            hidden state outputs for each word in the context for the claim.
        e (Variable) ∈ (N, L, H): \
            hidden state outputs for each word in the context for the evidence.
        
        N is batch size. T/L is number of words in the given sentence. H is hidden state size.
    Returns:
        Similarity Matrix ∈ (N, T, L). 
        S[n,t,j] is the sim between the t^th claim word and the j^th word for the n^th batch.
    """
    claim_tiled = claim.unsqueeze(2).expand(
        claim.size()[0],
        claim.size()[1],
        evidence.size()[1],
        claim.size()[2])
    evidence_tiled = evidence.unsqueeze(1).expand(
        evidence.size()[0],
        claim.size()[1],
        evidence.size()[1],
        evidence.size()[2])
    mtx = self.similarity_function(claim_tiled, evidence_tiled)
    if len(mtx.size()) < 3:
        mtx = mtx.unsqueeze(0)
    return mtx
### tests ###

def test_similarity_matrix_shape():
  N = 10
  T = 7
  J = 8
  H = 100
  c = Variable(torch.rand(N, T, H))
  e = Variable(torch.rand(N, J, H))
  s = SimilarityMatrix(hidden_size=H)
  mtx = s(c, e)
  assert mtx.size() == torch.Size((N,T,J))

def test_similarity_matrix_values():
  import numpy as np

  N = 10
  T = 7
  J = 8
  H = 100
  c = Variable(torch.rand(N, T, H))
  e = Variable(torch.rand(N, J, H))
  s = SimilarityMatrix(hidden_size=H)
  
  mtx = s(c, e)

  test_mtx_1 =  __test_get_sim_matrix_vec_1(s, c, e)
  test_mtx_2 =  __test_get_sim_matrix_vec_2(s, c, e)

  np.testing.assert_almost_equal(torch.sum(test_mtx_1 - test_mtx_2).data[0], 0, decimal=5)
  np.testing.assert_almost_equal(torch.sum(mtx - test_mtx_2).data[0], 0, decimal=5)
  np.testing.assert_almost_equal(torch.sum(test_mtx_1 - mtx).data[0], 0, decimal=5)

def __test_get_sim_matrix_vec_1(s, c, e):
  N, T, H = c.size()
  _, J, _ = e.size()
  mat = Variable(torch.zeros(N, T, J))
  for t in range(T):
    for j in range(J):
      mat[:,t,j] = s.similarity_function(c[:,t,:],e[:,j,:])
  return mat

def __test_get_sim_matrix_vec_2(s, c, e):
  N, T, H = c.size()
  _, J, _ = e.size()
  mat = Variable(torch.zeros(N, T, J))

  for t in range(T):
    # create representation across N for a given index.
    joined_rep = Variable(torch.zeros(N, J, H*3), requires_grad=False)
    # select claim across N.
    _c = c[:,t,:]
    # join each e representation to current c representation.
    for j in range(J):
      _e = e[:,j,:]
      joined_rep[:,j,:] = s.join(_c, _e)
    # place similarities in matrix
    mat[:,t,:] = s.activation(s.similarity(joined_rep))

  return mat
