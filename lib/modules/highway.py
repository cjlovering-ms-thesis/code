import torch

class Highway(torch.nn.Module):
  def __init__(self, input_size: int=80, num_layers: int=1, bias: float=1.0) -> None:
    """Initialize all layers.
    
    Args:
        input_size (int): size of hidden state of recurrent networks.
        num_layers (int): number of contiguous layers.
        bias (float): default value for bias.
    """
    super(Highway, self).__init__()
    self.activation = torch.nn.Sigmoid()
    self._gates = torch.nn.ModuleList([ torch.nn.Linear(input_size, input_size) for _ in range(num_layers) ])
    self._encoders = torch.nn.ModuleList([ torch.nn.Linear(input_size, input_size) for _ in range(num_layers) ])
    for gate in self._gates:
        gate.bias.data.fill_(bias)
    self.layers = zip(self._gates, self._encoders)
    
  def forward(self, x: torch.autograd.Variable) -> torch.autograd.Variable:
    """Applies highway to variable.

    Args:
        x (torch.autograd.Variable) ∈ (N,?,I): Input variable.
    Returns:
        (torch.autograd.Variable) ∈ (N,?,I): Output representation.
    """
    for gate, affine in self.layers:
        T = self.activation(gate(x))
        x = torch.mul(T, x) + torch.mul(1. - T, affine(x))
    return x
