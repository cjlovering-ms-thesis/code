from modules.highway import Highway
from modules.attention import Attention
from modules.similarity_matrix import SimilarityMatrix

import torch
import torchtext.vocab as vocab
import numpy as np

from torch.autograd import Variable

class BiDafR(torch.nn.Module):
  """BiDaf-r: this model shares a recurrent network for the claim and the evidence. """
  def __init__(self, word_emb_dim: int=200, hidden_size: int=100, out_dim: int=1, emb_vocab=vocab.GloVe,
      encoder: torch.nn.Module=torch.nn.LSTM, num_enc_layers: int=1, bidirectional: bool=False, dropout: float=0.,
      highway_layers_wrd: int=0, highway_layers_seq: int=0, attend_claim: bool=False, 
      attend_evidence: bool=False, fc_dim: int=200) -> None:
    """Initialize all layers.

    Args:
        word_emb_dim (int): size of word embedding.
        hidden_size (int): size of hidden state of recurrent networks.
        out_dim (int): the number of out dim (for us always 1).
        emb_vocab (vocab): the type of vocab (Glove vs word2vec).
        rec_net (torch.nn): the type of recurrent network (RNN, Glove, word2vec).
        bidirectional (bool): if true recurrent networks are bi-directional.
        attention (bool): if true add attention
    """
    super(BiDafR, self).__init__()
    self.word_emb_dim = word_emb_dim
    self.vocab = emb_vocab(name='6B', dim=self.word_emb_dim)
    self._volatile = False
    
    # if highway init highway layers for words
    self.highway_wrd = highway_layers_wrd > 0
    if self.highway_wrd:
      self.highway_wrd_network = Highway(input_size=word_emb_dim, num_layers=highway_layers_wrd)

    # determine how big final layer is.
    context_size = hidden_size * 3
    similarity_size = hidden_size

    # if bidirectional, reduce hidden size by x2 to keep # of params.
    hidden_size = hidden_size // 2 if bidirectional else hidden_size

    # sentence context encoders
    self.c_seq_encoder = encoder(input_size=self.word_emb_dim, hidden_size=hidden_size, \
        num_layers=num_enc_layers, bidirectional=bidirectional, dropout=dropout, batch_first=True)
    self.e_seq_encoder = encoder(input_size=self.word_emb_dim, hidden_size=hidden_size, \
        num_layers=num_enc_layers, bidirectional=bidirectional, dropout=dropout, batch_first=True)

    # attention
    self.attend_claim = attend_claim
    self.attend_evidence = attend_evidence
    if attend_claim or attend_evidence:
      self.similarity_matrix = SimilarityMatrix(hidden_size=similarity_size)
      self.attend = Attention()

    # if highway init highway layers for seq representation
    self.highway_seq = highway_layers_seq > 0
    if self.highway_seq:
      self.highway_seq_network = Highway(input_size=context_size, num_layers=highway_layers_seq)

    self.final_sequence = nn.Sequential(
      nn.Dropout(p=dropout),
      nn.Linear(context_size, fc_dim),
      nn.Tanh(),
      nn.Dropout(p=dropout),
      nn.Linear(fc_dim, fc_dim),
      nn.Tanh(),
      nn.Dropout(p=dropout),
      torch.nn.Linear(fc_dim, out_dim, bias=False),
    )
    

  def forward(self, x):
    """Forward pass on input sentences, predicting a score for each pair of claim & evidence.

    Args:
        x - List(List(string)): Each string is a sentence.
    """
    # split into claim and evidence.
    c, e = x

    # embed all words using chosen embedding (default: GloVe).
    # --> batch=32, seq_len=128, size=80
    c = self.get_batch(self.prepare_batch(c))
    e = self.get_batch(self.prepare_batch(e))

    # setup highway for encoding words.
    if self.highway_wrd:
      c = self.highway_wrd_network(c)
      e = self.highway_wrd_network(e)

    # process sentences through a sentence encoder (default: LSTM).
    # --> (batch=32, seq_len=128, size=80),(directions=1/2, batch=32, size=80)
    c_u, (c_u_l, _) = self.c_seq_encoder(c)
    e_u, (e_u_l, _) = self.e_seq_encoder(e)

    features = []

    if self.attend_claim or self.attend_evidence:
      mtx = self.similarity_matrix(c_u, e_u)

    if self.attend_claim:
      c_a = self.attend(c_u, mtx)
      features.append(c_a)
    else:
      features.append(torch.cat(list(c_u_l),1))

    if self.attend_evidence:
      e_a = self.attend(e_u, mtx)
      features.append(e_a)
    else:
      features.append(torch.cat(list(e_u_l),1))

    features = torch.cat([*features, torch.mul(*features)], 1)

    if self.highway_seq:
      features = self.highway_seq_network(features)

    return self.final_sequence(features)

  def prepare_batch(self, x):
    """ Args: x - [string, ... ] """
    split = [ s.split() for s in x ]
    filtered = [
        [ w for w in s if w in self.vocab.stoi ]
        for s in x
    ]
    MAX_SIZE = 128
    shortened = [
        s[:MAX_SIZE] for s in filtered
    ]
    return shortened

  def get_batch(self, batch):
    """Embeds a batch of sentences.

    Returns:
      (Variable): Embedded word matrix.
      (IntTensor): Lengths of each sentence.
    """
    batch_size = len(batch)
    max_len = max([len(s) for s in batch])
    embed = torch.zeros(batch_size, max_len, self.word_emb_dim)

    for i in range(batch_size):
      for j in range(len(batch[i])):
        embed[i, j, :] = self.get_word(batch[i][j])

    if torch.cuda.is_available():
        return Variable(torch.FloatTensor(embed), volatile=self._volatile).cuda()
    return Variable(torch.FloatTensor(embed), volatile=self._volatile)

  def get_word(self, word):
    """Returns the embedding for a word. """
    if word not in self.vocab.stoi:
        return self.vocab.vectors[0]
    return self.vocab.vectors[self.vocab.stoi[word]]

  def volatile(self, volatile):
    """Sets state to generate volatile vecotors. """
    self._volatile = volatile