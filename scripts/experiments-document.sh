python3 src/experiments_document.py --config_mode "basic" --log_file "experiment_results_document_rnn"
python3 src/experiments_document.py --config_mode "sen_attention" --log_file "experiment_results_document_attention"

python3 src/experiments_document.py --config_mode "basic" --log_file "experiment_results_document_argument_rnn" --in_path "../data/results/argument/"
python3 src/experiments_document.py --config_mode "sen_attention" --log_file "experiment_results_document_argument_attention" --in_path "../data/results/argument/"

python3 src/experiments_document.py --config_mode "wrd_attention" --log_file "experiment_results_document_birnn"
python3 src/experiments_document.py --config_mode "full_attention" --log_file "experiment_results_document_attention"

python3 src/experiments_document.py --config_mode "full_attention" --log_file "experiment_results_document_argument_attention" --in_path "../data/results/argument/"
python3 src/experiments_document.py --config_mode "wrd_attention" --log_file "experiment_results_document_argument_birnn" --in_path "../data/results/argument/"
