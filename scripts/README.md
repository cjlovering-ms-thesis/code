# Scripts

Utility scripts for calling various python3 scripts.

1. `aggregate.sh` generates the aggregated (inferred) pairwise dataset.
2. `clean_checkpoints.sh` deletes saved models and results.
3. `labels.sh` DEPRECATED. Used for comparing results in vote vs GLAD.
4. `process.sh` processes dataset and extracts answers.
5. `splits.sh` creates three different versions of the annotation.
6. `test.sh` runs tests.
