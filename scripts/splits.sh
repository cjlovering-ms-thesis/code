echo "unanimous"
mkdir -p ../data/results/unanimous/
python3 utils/extract.py --unanimous --out_path "../data/results/unanimous/"
echo "high"
mkdir -p ../data/results/high/
python3 utils/extract.py --high_majority --out_path "../data/results/high/"
echo "majority"
mkdir -p ../data/results/majority/
python3 utils/extract.py --majority --out_path "../data/results/majority/"
echo "exact-majority"
mkdir -p ../data/results/exact-majority/
python3 utils/extract.py --exact_majority --out_path "../data/results/exact-majority/"
echo "exact-high-majority"
mkdir -p ../data/results/exact-high-majority/
python3 utils/extract.py --exact_high_majority --out_path "../data/results/exact-high-majority/"
echo "high document"
mkdir -p ../data/results/document/
python3 utils/extract.py --document_representation True --high_majority --out_path "../data/results/document/"
echo "argument"
mkdir -p ../data/results/argument/
python3 utils/extract_argument.py
echo "aggregated"
mkdir -p ../data/results/infer/
python3 utils/aggregate.py --out_path "../data/results/infer/"