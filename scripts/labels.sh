#!/bin/sh

echo "This is used for labelling for comparing GLAD and majority vote."
convert()
{
    TARGET=$1
    OUTPUT=$2
    DATA=$3
    MODE=$4

    echo "Normalizing $MODE into .dat files for glad."
    python utils/normalize_mturk.py $DATA/$TARGET $DATA/$OUTPUT --tasks data/tracking/tasks.json --workers data/tracking/workers.json

    echo "Sampling labels."
    python utils/sample.py $DATA/$OUTPUT $DATA/3_$OUTPUT --sample 3
    python utils/sample.py $DATA/$OUTPUT $DATA/4_$OUTPUT --sample 4

    echo "Integrating $MODE into integrated labels."
    python utils/glad.py $DATA/3_$OUTPUT $DATA/3_glad_$OUTPUT
    python utils/glad.py $DATA/4_$OUTPUT $DATA/4_glad_$OUTPUT
    python utils/glad.py $DATA/$OUTPUT   $DATA/5_glad_$OUTPUT

    echo "Voting on $MODE turning them into labels."
    python utils/vote.py $DATA/3_$OUTPUT $DATA/3_vote_$OUTPUT
    python utils/vote.py $DATA/$OUTPUT   $DATA/5_vote_$OUTPUT    
}

convert response.csv response.dat data/results/0 response
