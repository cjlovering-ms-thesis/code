echo "Normalizing all sentence files."
python3 utils/process.py
echo "Embedding corpus. (~1hr)."
python3 utils/embed_corpus.py
echo "Create dataset. (~1min)."
python3 utils/create_dataset.py
