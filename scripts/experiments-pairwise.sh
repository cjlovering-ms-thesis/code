python3 src/experiments_pairwise.py --config_mode "rnn" --log_file "experiment_results_pairwise_argument_rnn" --in_path "../data/results/argument/"
python3 src/experiments_pairwise.py --config_mode "birnn" --log_file "experiment_results_pairwise_argument_birnn" --in_path "../data/results/argument/"
python3 src/experiments_pairwise.py --config_mode "attention" --log_file "experiment_results_pairwise_argument_attention" --in_path "../data/results/argument/"

# python3 src/experiments_pairwise.py --config_mode "rnn" --log_file "experiment_results_pairwise_infer_rnn" --in_path "../data/results/infer/"
# python3 src/experiments_pairwise.py --config_mode "rnn" --log_file "experiment_results_pairwise_majority_rnn" --in_path "../data/results/majority/" 
# python3 src/experiments_pairwise.py --config_mode "rnn" --log_file "experiment_results_pairwise_high_rnn" --in_path "../data/results/high/"
# python3 src/experiments_pairwise.py --config_mode "rnn" --log_file "experiment_results_pairwise_unanimous_rnn" --in_path "../data/results/unanimous/"

# python3 src/experiments_pairwise.py --config_mode "birnn" --log_file "experiment_results_pairwise_infer_birnn" --in_path "../data/results/infer/"
# python3 src/experiments_pairwise.py --config_mode "birnn" --log_file "experiment_results_pairwise_majority_birnn" --in_path "../data/results/majority/"
# python3 src/experiments_pairwise.py --config_mode "birnn" --log_file "experiment_results_pairwise_high_birnn" --in_path "../data/results/high/"
# python3 src/experiments_pairwise.py --config_mode "birnn" --log_file "experiment_results_pairwise_unanimous_birnn" --in_path "../data/results/unanimous/"

# python3 src/experiments_pairwise.py --config_mode "attention" --log_file "experiment_results_pairwise_infer_attention" --in_path "../data/results/infer/"
# python3 src/experiments_pairwise.py --config_mode "attention" --log_file "experiment_results_pairwise_majority_attention" --in_path "../data/results/majority/"
# python3 src/experiments_pairwise.py --config_mode "attention" --log_file "experiment_results_pairwise_high_attention" --in_path "../data/results/high/"
# python3 src/experiments_pairwise.py --config_mode "attention" --log_file "experiment_results_pairwise_unanimous_attention" --in_path "../data/results/unanimous/"