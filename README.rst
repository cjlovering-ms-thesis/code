Why did they cite that?
=======================

Enclose this workspace in a project folder, i.e. `why-did-you-cite-this`. 

View https://bitbucket.org/cjlovering-ms-thesis/overview to see how to use this library with jupyter notebook on a remote machine.

Setup
-----

To setup::

  # installs requirements and data.
  make init
  # check that it worked.
  make test

For work before the annotation of the dataset::

  make prepare_corpus         # embeds corpus.
  make create_gold_questions  # creates an outline for gold standard questions.
  # manually label questions.
  make create_dataset         # creates tasks for mechanical turk.
  # upload tasks to mturk.
  make prepare_dataset        # aggregates multiple annotations.
Further Dependencies
--------------------

In order embed raw text for creating the dataset for mechanical turk download the following resources. This is not necessary if you wish to use either our generated dataset, or our labeled dataset.

Install InferSent::

  cd .. # from code
  git clone https://github.com/facebookresearch/InferSent

Then download `infersent.allnli.pickle` if you don't wish to retrain InferSent,
and move it as follows into `resources`::

  cd .. # from code
  mkdir resources
  mv path/to/pickle resources/





